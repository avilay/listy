Scenarios
=========
New user registration with happy@orange.com as email

New user registration
Create a new task in the (Default) list
Schedule the new task
Create a new scheduled task in the (Default) list
Creates a new list
Log out

Cookie (with empty lists) logs in and sees an empty scheduled list
Clicks on (Default) and sees an empty list
Creates new task in (Default)
Creates another scheduled task in (Default)
Goes back to Scheduled and sees the scheduled task show up
Edits the first task to schedule it.
Goes back to Scheduled and sees two tasks show up
Logs out

Frozen (with tasks only in (Default) list but nothing scheduled) logs in and sees empty scheduled list
Clicks on (Default) and sees the list
Schedules the first task
Add estimates to another task
Logs out

Happy logs in and sees a full scheduled list
Marks the first task as done
Looks at some other list
Creates a new scheduled task
Creates a new unscheduled task
Schedules a previously unscheduled task
Comes back to Scheduled, first task is still showing up as done, 2 new scheduled tasks are showing up
Edits the second task and un-schedules it
Goes back to Scheduled and second task has disappearerd
Edits a third task and changes the schedule date
Goes back to Scheduled to see that reflected
Pauses both in-progress tasks
Starts a new task
Resumes a paused task
Log out

