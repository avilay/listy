#!/bin/bash
# To build -
# Decide on the release number
# Change the release number in prod.nginx.conf
# Run this script from the ops directory
# On the server -
# sudo nginx -s quit
# ps aux | grep gunicorn
# kill -9 ...
# mv dist*.zip /data/www/listy
# cd /data/www/listy
# unzip dist.${RELEASE}.zip
# mv dist.${RELEASE}/nginx.conf /etc/nginx
# cd /data/www/listy/dist.${RELEASE}
# gunicorn api.api:app --pid /data/www/listy/api_gunicorn.pid --daemon --timeout 60 --bind 127.0.0.1:7777 --log-file /etc/listy/logs/api_gunicorn.log --log-level debug --worker-class aiohttp.worker.GunicornWebWorker
# Check that /data/www/listy/api_gunicorn.pid exists
# sudo nginx

RELEASE=1.1
cd ../web
rm -fr build
npm run build
cd ..
rm -fr dist.*
mkdir dist.${RELEASE}
cp -R api dist.${RELEASE}
cp -R web/build dist.${RELEASE}/web
cp ops/prod.nginx.conf dist.${RELEASE}/nginx.conf
zip -r dist.${RELEASE}.zip dist.${RELEASE}
scp -i ~/keys/ec2_keys.pem dist.${RELEASE}.zip ubuntu@avilay.info:~
