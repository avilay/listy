#/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
/Applications/Postgres.app/Contents/Versions/9.5/bin/psql -d listy -f $DIR/../api/repository/pg/drop.sql
/Applications/Postgres.app/Contents/Versions/9.5/bin/psql -d listy -f $DIR/../api/repository/pg/create.sql
python $DIR/../api/test/pg/gen_test_data.py
