import json
from avilabsutils import JsonConverter


def gen_json_dumper():
    def json_dumper(*args, **kwargs):
        return json.dumps(*args, **kwargs, cls=JsonConverter)
    return json_dumper



