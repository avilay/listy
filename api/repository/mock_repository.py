from datetime import datetime
from api.models.user import User


class MockRepository:
    def get_user(self, userid=None, name=None):
        if userid:
            pass
        elif name:
            return User(
                id=1,
                name='Happy Orange',
                email='happy@orange.com',
                password_hash='$2b$12$IwttWapjqu5SxvBQ/JiKoOGNKwvcgSte5Daokxa3wQRqLdVSbgzzC',
                created_on=datetime(2016, 1, 1)
            )
        elif userid and name:
            pass
        else:
            raise RuntimeError('At least one of userid or name MUST be provided!')
