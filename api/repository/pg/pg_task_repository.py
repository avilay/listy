from datetime import datetime, timezone

from avilabsutils.decorators import InvalidArgError, InvalidArgValueError

from api.models import Task
from ..repo_exceptions import DataNotFoundError, NotEnoughArgsError
from .pg_repository import PgRepository, validate_method_args


def check_task_attrs(task_attrs):
    all_attrs = {'title', 'estimated_minutes', 'description', 'scheduled_start_on', 'state'}
    given_attrs = set(task_attrs.keys())
    state = task_attrs.get('state', 'n/a')
    # user can only set these states; not-started and scheduled are set by system
    return state in ['in-progress', 'paused', 'done', 'n/a'] and len(all_attrs.intersection(given_attrs)) > 0


class PgTaskRepository(PgRepository):

    def _owns(self, user_id, list_id):
        sql = 'SELECT COUNT(*) FROM lists WHERE user_id = %s AND id = %s'
        cur = self._cursor()
        cur.execute(sql, [user_id, list_id])
        row = cur.fetchone()
        return row['count'] > 0

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0)
    ])
    def get_tasks(self, user_id, list_id):
        sql = '''
        SELECT t.id AS id, title, t.state AS state, t.created_on AS created_on, minutes_spent, scheduled_start_on,
        estimated_minutes, description, completed_on, last_start_on, list_id, l.name as list_name
        FROM lists l, tasks t
        WHERE l.id = t.list_id
        AND l.user_id = %s
        AND l.id = %s
        ORDER BY t.id
        '''
        cur = self._cursor()
        cur.execute(sql, [user_id, list_id])
        if cur.rowcount == 0:
            if self._owns(user_id, list_id):
                return []
            else:
                raise DataNotFoundError('User id {} does not have a list with id {}'.format(user_id, list_id))
        rows = cur.fetchall()
        cur.close()
        return [Task(**row) for row in rows]

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0),
        ('recent', int, lambda x: x > 0)
    ])
    def get_recent_tasks(self, user_id, list_id, recent=3):
        sql = '''
        SELECT t.id AS id, title, t.state AS state, t.created_on AS created_on, minutes_spent, scheduled_start_on,
        estimated_minutes, description, completed_on, last_start_on, list_id, l.name as list_name
        FROM tasks t
        INNER JOIN lists l
        ON t.list_id = l.id
        WHERE l.user_id = %s
        AND l.id = %s
        AND (
            (current_timestamp - completed_on) <= interval '%s days'
            OR completed_on IS NULL
        )
        ORDER BY t.id
        '''
        cur = self._cursor()
        cur.execute(sql, [user_id, list_id, recent])
        if cur.rowcount == 0:
            if self._owns(user_id, list_id):
                return []
            else:
                raise DataNotFoundError('User id {} does not have a list with id {}'.format(user_id, list_id))
        rows = cur.fetchall()
        cur.close()
        return [Task(**row) for row in rows]

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0),
        ('task_id', int, lambda x: x > 0)
    ])
    def get_task_details(self, user_id, list_id, task_id):
        sql = '''
        SELECT t.id AS id, title, t.state AS state, t.created_on AS created_on, minutes_spent, scheduled_start_on,
        estimated_minutes, last_start_on, list_id, l.name as list_name, t.description, t.last_start_on, t.completed_on
        FROM tasks t
        INNER JOIN lists l
        ON t.list_id = l.id
        WHERE l.user_id = %s
        AND l.id = %s
        AND t.id = %s
        '''
        cur = self._cursor()
        cur.execute(sql, [user_id, list_id, task_id])
        if cur.rowcount == 0:
            raise DataNotFoundError('User id {} with list id {} and task id {} does not exist'.format(user_id, list_id, task_id))
        row = cur.fetchone()
        cur.close()
        return Task(**row)

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0),
        ('title', str, lambda x: x != ''),
        ('scheduled_start_on', datetime, None),
        ('estimated_minutes', int, lambda x: x > 0),
        ('description', str, lambda x: x != '')
    ])
    def create_task(self, user_id, list_id, title, scheduled_start_on=None, estimated_minutes=0, description=''):
        now = datetime.now(timezone.utc)
        if scheduled_start_on:
            sql = '''
            INSERT INTO tasks (list_id, title, estimated_minutes, description, created_on, scheduled_start_on, state)
            VALUES (%s, %s, %s, %s, %s, %s, %s)
            RETURNING *
            '''
            params = [list_id, title, estimated_minutes, description, now, scheduled_start_on, 'scheduled']
        else:
            sql = '''
            INSERT INTO tasks (list_id, title, estimated_minutes, description, created_on)
            VALUES (%s, %s, %s, %s, %s)
            RETURNING *
            '''
            params = [list_id, title, estimated_minutes, description, now]
        if self._owns(user_id, list_id):
            cur = self._cursor()
            cur.execute('SELECT name FROM lists WHERE id = %s', [list_id])
            list_name = cur.fetchone()['name']
            cur.execute(sql, params)
            row = cur.fetchone()
            cur.close()
            row['list_name'] = list_name
            return Task(**row)
        else:
            raise DataNotFoundError('User id {} with list id {} does not exist'.format(user_id, list_id))

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0),
        ('task_id', int, lambda x: x > 0)
    ])
    def delete_task(self, user_id, list_id, task_id):
        sql = 'DELETE FROM tasks WHERE list_id = %s AND id = %s'
        if self._owns(user_id, list_id):
            cur = self._cursor()
            cur.execute(sql, [list_id, task_id])
            if cur.rowcount == 0:
                cur.close()
                raise DataNotFoundError('List id {} with task id {} does not exist'.format(list_id, task_id))
        else:
            raise DataNotFoundError('User id {} with list id {} does not exist'.format(user_id, list_id))

    @validate_method_args([
        ('user_id', int, lambda x: x > 0)
    ])
    def get_scheduled_tasks(self, user_id):
        sql = '''
        SELECT t.id AS id, title, t.state AS state, t.created_on AS created_on, minutes_spent, scheduled_start_on,
        estimated_minutes, last_start_on, list_id, l.name as list_name
        FROM tasks t
        INNER JOIN lists l ON t.list_id = l.id
        INNER JOIN users u ON l.user_id = u.id
        WHERE u.id = %s
        AND t.state IN ('scheduled', 'in-progress', 'paused')
        AND l.state = 'active'
        ORDER BY scheduled_start_on
        '''
        cur = self._cursor()
        cur.execute(sql, [user_id])
        rows = cur.fetchall()
        cur.close()
        return [Task(**row) for row in rows]

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0),
        ('task_id', int, lambda x: x > 0),
        ('task_attrs', dict, check_task_attrs)
    ])
    def update_task(self, user_id, list_id, task_id, task_attrs):
        now = datetime.now(timezone.utc)
        sql = '''
        UPDATE tasks SET {}
        WHERE list_id = %s
        AND id = %s
        RETURNING *
        '''
        if not self._owns(user_id, list_id):
            raise DataNotFoundError('User id {} with list id {} does not exist'.format(user_id, list_id))
        cur = self._cursor()
        cur.execute('SELECT * FROM tasks WHERE id = %s AND list_id = %s', [task_id, list_id])
        if cur.rowcount == 0:
            raise DataNotFoundError('List id {} does not have task id {}'.format(list_id, task_id))
        row = cur.fetchone()
        task_details = Task(**row)
        qry = ''
        params = []

        if 'title' in task_attrs:
            if task_attrs['title']:
                qry += 'title = %s, '
                params.append(task_attrs['title'])
            else:
                raise InvalidArgValueError('Title cannot be empty!')

        if 'scheduled_start_on' in task_attrs:
            qry += 'scheduled_start_on = %s, '
            params.append(task_attrs['scheduled_start_on'])
            if task_attrs['scheduled_start_on'] is not None:
                if 'state' in task_attrs:
                    raise InvalidArgError('Cannot set scheduled_start_on with some other state')
                task_attrs['state'] = 'scheduled'
            else:
                self._logger.debug('sched start on is present but is null, setting state to not-started')
                task_attrs['state'] = 'not-started'

        if 'estimated_minutes' in task_attrs:
            qry += 'estimated_minutes = %s, '
            params.append(task_attrs['estimated_minutes'])

        if 'description' in task_attrs:
            qry += 'description = %s, '
            params.append(task_attrs['description'])

        if 'state' in task_attrs:
            # not-started ==> scheduled
            #             ==> done
            #             ==> in-progress: set last_start_on
            #
            # scheduled   ==> in-progress: set last_start_on
            #             ==> done: set completed_on
            #             ==> scheduled: no-op
            #             ==> not-started: no-op
            #
            # in-progress ==> done: increment minutes_spent, set completed_on
            #             ==> paused: increment minutes_spent
            #
            # paused      ==> in-progress: set last_start_on
            #             ==> done: set completed_on
            state = task_attrs['state']
            old_state = task_details.state

            if old_state == 'not-started' and state == 'scheduled':
                qry += 'state = %s, '
                params.append(state)
            elif old_state == 'not-started' and state == 'done':
                qry += 'state = %s, completed_on = %s, '
                params += [state, now]
            elif old_state == 'not-started' and state == 'in-progress':
                qry += 'state = %s, last_start_on = %s'
                params += [state, now]

            elif old_state == 'scheduled' and state == 'in-progress':
                qry += 'state = %s, last_start_on = %s, '
                params += [state, now]
            elif old_state == 'scheduled' and state == 'done':
                qry += 'state = %s, completed_on = %s, '
                params += [state, now]
            elif old_state == 'scheduled' and state == 'scheduled':
                pass
            elif old_state == 'scheduled' and state == 'not-started':
                qry += 'state = %s, '
                params.append(state)

            elif old_state == 'in-progress' and state == 'done':
                ms = task_details.minutes_spent + (now - task_details.last_start_on).total_seconds() / 60
                qry += 'state = %s, minutes_spent = %s, completed_on = %s, '
                params += [state, ms, now]
            elif old_state == 'in-progress' and state == 'paused':
                ms = task_details.minutes_spent + (now - task_details.last_start_on).total_seconds() / 60
                qry += 'state = %s, minutes_spent = %s, '
                params += [state, ms]

            elif old_state == 'paused' and state == 'in-progress':
                qry += 'state = %s, last_start_on = %s, '
                params += [state, now]
            elif old_state == 'paused' and state == 'done':
                qry += 'state = %s, completed_on = %s, '
                params += [state, now]
            else:
                raise InvalidArgValueError('Invalid state transition from {} to {}'.format(old_state, state))
        qry = qry.rstrip(', ')
        if len(params) == 0:
            raise NotEnoughArgsError()
        sql = sql.format(qry)
        params += [list_id, task_id]
        cur.execute(sql, params)
        row = cur.fetchone()
        cur.execute('SELECT name FROM lists WHERE id = %s', [list_id])
        list_name = cur.fetchone()['name']
        cur.close()
        row['list_name'] = list_name
        return Task(**row)












