CREATE TABLE IF NOT EXISTS users (
    id serial NOT NULL CONSTRAINT u_pri_key PRIMARY KEY,
    name varchar(50) NOT NULL,
    email varchar(50) NOT NULL UNIQUE,
    password_hash varchar(128) NOT NULL,
    created_on timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(2)    
);

CREATE TYPE list_state AS ENUM ('active', 'archived');

CREATE TABLE IF NOT EXISTS lists (
    id serial NOT NULL CONSTRAINT l_pri_key PRIMARY KEY,
    user_id integer NOT NULL CONSTRAINT fk_l_users REFERENCES users ON DELETE CASCADE,
    name varchar(256) NOT NULL,
    state list_state NOT NULL DEFAULT 'active',
    created_on timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(2),
    updated_on timestamptz
);

CREATE TYPE task_state AS ENUM ('not-started', 'scheduled', 'in-progress', 'paused', 'done');

CREATE TABLE IF NOT EXISTS tasks (
    id serial NOT NULL CONSTRAINT t_pri_key PRIMARY KEY,
    list_id integer NOT NULL CONSTRAINT fk_t_lists REFERENCES lists ON DELETE CASCADE,
    title varchar(256) NOT NULL,
    state task_state NOT NULL DEFAULT 'not-started',
    created_on timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(2),
    minutes_spent integer NOT NULL DEFAULT 0,
    description text,
    scheduled_start_on timestamptz,
    last_start_on timestamptz,
    estimated_minutes integer,
    completed_on timestamptz
);

