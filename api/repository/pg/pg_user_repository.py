from datetime import datetime, timezone

from avilabsutils.decorators import InvalidArgValueError

from api.models import User
from psycopg2 import IntegrityError
from api.repository.pg import PgListRepository
from .pg_repository import PgRepository, validate_method_args
from api.repository.repo_exceptions import NotEnoughArgsError


class PgUserRepository(PgRepository):
    @validate_method_args([
        ('name', str, lambda x: x),
        ('email', str, lambda x: x.find('@') > -1),
        ('password', str, lambda x: x)
    ])
    def create_user(self, name, email, password):
        now = datetime.now(timezone.utc)
        sql = '''
        INSERT INTO users (name, email, password_hash, created_on)
        VALUES (%s, %s, %s, %s)
        RETURNING *
        '''
        hashed_pwd = User.hash_password(password)
        try:
            cur = self._cursor()
            cur.execute(sql, [name, email, hashed_pwd, now])
            row = cur.fetchone()
            user = User(**row)

            sql = '''
            INSERT INTO lists (user_id, name, state, created_on, updated_on)
            VALUES (%s, %s, 'active', %s, %s)
            '''
            cur.execute(sql, [user.id, PgListRepository.DEFAULT_LIST_NAME, now, now])
            cur.close()

            return user
        except IntegrityError as ie:
            raise InvalidArgValueError(ie)

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('email', str, None)
    ])
    def get_user(self, user_id=None, email=''):
        """
        At least one of an existing user_id or email must be provided.
        """
        if not user_id and not email:
            raise NotEnoughArgsError('One of user_id or email must be present')

        sql = 'SELECT id, name, email, password_hash, created_on FROM users'
        params = None
        errmsg = None
        if user_id and not email:
            sql += ' WHERE id = %s'
            params = [user_id]
            errmsg = 'User id {} does not exist!'.format(user_id)
        elif not user_id and email:
            sql += ' WHERE email = %s'
            params = [email]
            errmsg = 'Email {} does not exist!'.format(email)
        elif user_id and email:
            sql += ' WHERE id = %s AND email = %s'
            params = [user_id, email]
            errmsg = 'User id {} and email {} do not exist!'.format(user_id, email)

        cur = self._cursor()
        cur.execute(sql, params)
        if cur.rowcount == 0:
            cur.close()
            raise LookupError(errmsg)
        row = cur.fetchone()
        cur.close()
        return User(**row)

    @validate_method_args([
        ('email', str, None),
        ('password', str, None)
    ])
    def find_user(self, email, password):
        sql = '''
        SELECT id, name, email, password_hash, created_on
        FROM users
        WHERE email = %s
        '''
        cur = self._cursor()
        cur.execute(sql, [email])
        if cur.rowcount == 0:
            return None
        row = cur.fetchone()
        user = User(**row)
        if user.check_password(password):
            return user
        else:
            return None
