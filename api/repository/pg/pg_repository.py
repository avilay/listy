import sys
from datetime import datetime, timezone
import shortuuid
import psycopg2 as pg
import psycopg2.extras as pgextras
from avilabsutils.decorators import *

from api.models import User, List, Task
from api.repository.repo_exceptions import NotEnoughArgsError, DataNotFoundError


class PgRepository:

    def __init__(self, config):
        self._logger = logging.getLogger(__package__)

        self._db = None
        self._dbparams = dict()

        if 'POSTGRES' not in config:
            raise RuntimeError('config file MUST contain POSTGRES section!')
        pgconfig = config['POSTGRES']

        if 'host' in pgconfig and 'user' in pgconfig and 'database' in pgconfig:
            self._dbparams['host'] = pgconfig['host']
            self._dbparams['user'] = pgconfig['user']
            self._dbparams['database'] = pgconfig['database']
            if 'port' in pgconfig:
                self._dbparams['port'] = pgconfig['port']
            if 'password' in pgconfig:
                self._dbparams['password'] = pgconfig['password']
        else:
            raise RuntimeError('config[POSTGRES] MUST define host, user, and database')

        self._connect()

    def close(self):
        if self._db and self._db.closed == 0:
            self._db.close()

    def _cursor(self):
        if self._db and self._db.closed != 0:
            self._logger.warn('PG connection is closed/broken. Re-connecting.')
            self._connect()
        return self._db.cursor(cursor_factory=pgextras.RealDictCursor)

    @retry(max_retries=3)
    def _connect(self):
        self._db = pg.connect(**self._dbparams)
        self._db.autocommit = True
