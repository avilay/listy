import sys
from datetime import datetime, timezone
import psycopg2 as pg
from avilabsutils.decorators import validate_method_args

from api.models import List
from .pg_repository import PgRepository, InvalidArgError

from api.repository.repo_exceptions import DataNotFoundError, NotEnoughArgsError


class PgListRepository(PgRepository):
    DEFAULT_LIST_NAME = '(Default)'

    @validate_method_args([
        ('user_id', int, lambda x: x > 0)
    ])
    def get_lists(self, user_id):
        sql = '''
        SELECT id, name, state, created_on, updated_on, user_id
        FROM lists
        WHERE user_id = %s
        '''
        cur = self._cursor()
        cur.execute(sql, [user_id])
        if cur.rowcount == 0:
            errmsg = 'User id {} does not exist!'.format(user_id)
            cur.close()
            raise DataNotFoundError(errmsg)
        rows = cur.fetchall()
        cur.close()
        return [List(**row) for row in rows]

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0)
    ])
    def get_list(self, user_id, list_id):
        sql = '''
        SELECT id, name, state, created_on, updated_on, user_id
        FROM lists
        WHERE user_id = %s
        AND id = %s
        '''
        cur = self._cursor()
        cur.execute(sql, [user_id, list_id])
        if cur.rowcount == 0:
            errmsg = 'User id {} does not own list id {}!'.format(user_id, list_id)
            cur.close()
            raise DataNotFoundError(errmsg)
        row = cur.fetchone()
        cur.close()
        return List(**row)

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_name', str, lambda x: x not in ['', '(Default)'])
    ])
    def create_list(self, user_id, list_name):
        """
        Cannot create a list named (Default)
        """
        now = datetime.now(timezone.utc)
        sql = '''
        INSERT INTO lists (name, user_id, state, created_on, updated_on)
        VALUES (%s, %s, 'active', %s, %s)
        RETURNING *
        '''
        cur = self._cursor()
        try:
            cur.execute(sql, [list_name, user_id, now, now])
            row = cur.fetchone()
            return List(**row)
        except pg.IntegrityError:
            cur.close()
            errmsg = 'User id {} does not exist'.format(user_id)
            raise DataNotFoundError(errmsg).with_traceback(sys.exc_info()[2])

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0),
        ('list_name', str, lambda x: x != '(Default)'),
        ('list_state', str, lambda x: x in ['archived', 'active', ''])
    ])
    def update_list(self, user_id, list_id, list_name='', list_state=''):
        if not list_name and not list_state:
            raise NotEnoughArgsError('One of list_name or list_state must be provided')

        now = datetime.now(timezone.utc)
        sql = '''
        UPDATE lists SET {}, updated_on = %s
        WHERE user_id = %s
        AND id = %s
        AND name != '(Default)'
        RETURNING *
        '''
        params = None
        if list_name and list_state:
            sql = sql.format('name = %s, state = %s')
            params = [list_name, list_state]
        elif list_name and not list_state:
            sql = sql.format('name = %s')
            params = [list_name]
        elif not list_name and list_state:
            sql = sql.format('state = %s')
            params = [list_state]
        params = params + [now, user_id, list_id]
        cur = self._cursor()
        cur.execute(sql, params)
        if cur.rowcount == 0:
            errmsg = 'User id {} with a non-default list id {} does not exist'.format(user_id, list_id)
            cur.close()
            raise DataNotFoundError(errmsg)
        row = cur.fetchone()
        cur.close()
        return List(**row)

    @validate_method_args([
        ('user_id', int, lambda x: x > 0),
        ('list_id', int, lambda x: x > 0)
    ])
    def delete_list(self, user_id, list_id):
        sql = "DELETE FROM lists WHERE user_id = %s AND id = %s AND name != '(Default)'"
        cur = self._cursor()
        cur.execute(sql, [user_id, list_id])
        if cur.rowcount == 0:
            cur.close()
            raise DataNotFoundError('User id {} with a non-default list id {} does not exist'.format(user_id, list_id))
        cur.close()
