class NotEnoughArgsError(Exception):
    pass


class DataNotFoundError(Exception):
    pass

