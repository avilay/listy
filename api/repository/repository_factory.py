from .pg import PgUserRepository, PgListRepository, PgTaskRepository


def create_task_repo(config):
    return PgTaskRepository(config)


def create_list_repo(config):
    return PgListRepository(config)


def create_user_repo(config):
    return PgUserRepository(config)
