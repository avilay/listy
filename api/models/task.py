# Valid states are not-started, scheduled, in-progress, paused, done
'''
self.description = kwargs.get('description', None)
        self.last_start_on = kwargs.get('last_start_on', None)
        self.completed_on = kwargs.get('completed_on', None)
'''
class Task:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)

        # calculated by system
        self.created_on = kwargs.get('created_on', None)
        self.minutes_spent = kwargs.get('minutes_spent', None)
        self.last_start_on = kwargs.get('last_start_on', None)
        self.completed_on = kwargs.get('completed_on', None)

        # specified by the user
        self.state = kwargs.get('state', None)
        self.title = kwargs.get('title', None)
        self.scheduled_start_on = kwargs.get('scheduled_start_on', None)
        self.estimated_minutes = kwargs.get('estimated_minutes', None)
        self.description = kwargs.get('description', None)

        self.list_id = kwargs.get('list_id', None)
        self.list_name = kwargs.get('list_name', None)

    def to_dict(self):
        return self.__dict__

    def __repr__(self):
        ret = '<TaskDetails(id={}, list_id={}, title={}, state={}, created_on={}, minutes_spent={}, scheduled_start_on={}, estimated_minutes={}, description={}, last_start_on={}, completed_on={})>'
        ret = ret.format(self.id, self.list_id, self.title, self.state, self.created_on, self.minutes_spent,
                         self.scheduled_start_on, self.estimated_minutes, self.description, self.last_start_on,
                         self.completed_on)
        return ret
