from datetime import timezone
import bcrypt


class User:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.name = kwargs.get('name', None)
        self.email = kwargs.get('email', None)
        self.password_hash = kwargs.get('password_hash', None)
        self.created_on = kwargs.get('created_on', None)

    def check_password(self, password):
        password = password.encode('utf-8')
        password_hash = self.password_hash.encode('utf-8')
        return bcrypt.hashpw(password, password_hash) == password_hash

    def to_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            email=self.email,
            created_on=self.created_on.astimezone(timezone.utc).isoformat() if self.created_on else None
        )

    def __repr__(self):
        return '<User(id={}, name={}, email={}, created_on={})>'.format(
            self.id, self.name, self.email, self.created_on
        )

    @staticmethod
    def hash_password(password):
        password = password.encode('utf-8')
        return bcrypt.hashpw(password, bcrypt.gensalt()).decode()

