class List:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.name = kwargs.get('name', None)
        self.state = kwargs.get('state', None)
        self.created_on = kwargs.get('created_on', None)
        self.updated_on = kwargs.get('updated_on', None)
        self.user_id = kwargs.get('user_id', None)

    def to_dict(self):
        return self.__dict__

    def __repr__(self):
        return '<List(id={}, name={}, state={}, created_on={}, updated_on={}, user_id={})>'.format(
            self.id,
            self.name,
            self.state,
            self.created_on,
            self.updated_on,
            self.user_id
        )
