import logging
from aiohttp import web


async def url_params_factory(app, handler):
    logger = logging.getLogger(__package__)

    async def url_params_handler(req):
        logger.info('Inside url_params_handler')
        try:
            if 'listid' in req.match_info:
                req['listid'] = int(req.match_info['listid'])
            if 'taskid' in req.match_info:
                req['taskid'] = int(req.match_info['taskid'])
        except ValueError:
            raise web.HTTPBadRequest(reason='ids must be a number')

        return await handler(req)

    return url_params_handler
