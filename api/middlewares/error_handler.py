import logging
import traceback
import sys
from aiohttp import web
from aiohttp.web_exceptions import HTTPError

from avilabsutils.decorators import InvalidArgError
from ..repository.repo_exceptions import NotEnoughArgsError, DataNotFoundError


async def error_handler_factory(app, handler):
    logger = logging.getLogger(__package__)

    async def error_handler(req):
        logger.info('Inside error handler')
        try:
            return await handler(req)
        except (InvalidArgError, NotEnoughArgsError) as err:
            logger.warn(str(err))
            logger.warn(traceback.print_tb(sys.exc_info()[2]))
            logger.debug('Return reason as {}'.format(err))
            raise web.HTTPBadRequest(reason=str(err))
        except DataNotFoundError as le:
            logger.warn(str(le))
            logger.warn(traceback.print_tb(sys.exc_info()[2]))
            raise web.HTTPForbidden(reason=str(le))
        except HTTPError:
            raise
        except Exception as ex:
            logger.error(traceback.print_tb(sys.exc_info()[2]))
            logger.error('Unhandled exception: ' + str(ex))
            raise web.HTTPInternalServerError()

    return error_handler
