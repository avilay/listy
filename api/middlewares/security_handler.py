import logging
import base64
from aiohttp import web

PUBLIC_CALLS = [
    ('*', '/logs'),
    ('*', '/error'),
    ('*', '/hello'),
    ('OPTIONS', '*'),
    ('*', '/login'),
    ('*', '/register')
]


def is_open(method, path):
    for allowed_method, allowed_path in PUBLIC_CALLS:
        if allowed_method == '*' or allowed_method == method:
            if allowed_path == '*' or allowed_path == path:
                return True
    return False

def extract_creds(req):
    logger = logging.getLogger(__package__)
    try:
        auth = req.headers['AUTHORIZATION']
        tokens = auth.split()
        assert tokens[0] == 'Basic'
        assert len(tokens) == 2
        user, pwd = base64.b64decode(tokens[1]).decode('utf-8').split(':')
        return user, pwd
    except Exception as ex:
        logger.error(ex)
        logger.error('Basic authentication protocol incorrect!')
        return None, None


async def security_factory(app, handler):
    logger = logging.getLogger(__package__)

    async def security_handler(req):
        logger.info('Inside security_handler')

        if is_open(req.method, req.path):
            return await handler(req)

        username, pwd = extract_creds(req)
        if username and pwd:
            repo = app['user_repo']
            try:
                user = repo.get_user(email=username)
                if user.check_password(pwd):
                    req['user'] = user
                    req['password'] = pwd
                    return await handler(req)
            except LookupError as err:
                logger.error(str(err))
                raise web.HTTPUnauthorized()

        raise web.HTTPUnauthorized()

    return security_handler
