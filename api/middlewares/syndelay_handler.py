import logging
import time


async def syndelay_handler_factory(app, handler):
    logger = logging.getLogger(__package__)

    async def syndelay_handler(req):
        logger.info('Inside syndelay_handler')
        time.sleep(2)
        return await handler(req)

    return syndelay_handler