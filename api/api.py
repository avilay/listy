import logging
import os
import os.path as path
from configparser import ConfigParser
from aiohttp import web
import asyncio
import psycopg2 as pg
import aiohttp_cors

from .controllers.user_controller import UserController
from .controllers.list_controller import ListController
from .controllers.task_controller import TaskController
from .controllers.check_controller import CheckController
from .middlewares import security_factory, url_params_factory, error_handler_factory, syndelay_handler_factory
import api.repository.repository_factory as repository_factory

'''
listy$ gunicorn api.api:app \
> --pid ./api_gunicorn.pid \
> --daemon \
> --timeout 60 \
> --bind 127.0.0.1:7777 \
> --log-file /etc/listy/logs/api_gunicorn.log
> --log-level debug
> --worker-class aiohttp.worker.GunicornWebWorker
'''


def config_log(config):
    logformat = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
    formatter = logging.Formatter(logformat)

    fh = logging.FileHandler('/etc/listy/logs/api.log')
    fh.setFormatter(formatter)

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)

    loggers = [
        logging.getLogger(__package__),
        logging.getLogger('aiohttp.access'),
        logging.getLogger('aiohttp.client'),
        logging.getLogger('aiohttp.internal'),
        logging.getLogger('aiohttp.server')
    ]

    loglevel = getattr(logging, config['LOGGING']['filelevel'].upper())
    for logger in loggers:
        logger.setLevel(loglevel)
        logger.addHandler(fh)
        if loglevel == logging.DEBUG:
            logger.addHandler(ch)


def read_config():
    env = os.environ.get('ENV', 'dev')
    configfile = '/etc/listy/conf/api.{}.ini'.format(env)
    if not path.exists(configfile):
        raise RuntimeError('Config file {} does not exist!'.format(configfile))
    config = ConfigParser()
    config.read(configfile)
    return config


def init_func(args):
    config = read_config()
    config_log(config)

    app = web.Application(middlewares=[
        # syndelay_handler_factory,
        security_factory,
        url_params_factory,
        error_handler_factory
    ])

    app['user_repo'] = repository_factory.create_user_repo(config)
    app['list_repo'] = repository_factory.create_list_repo(config)
    app['task_repo'] = repository_factory.create_task_repo(config)
    app['user_repo'] = repository_factory.create_user_repo(config)

    # app['repo'] = MockRepository()

    check_controller = CheckController()
    check_controller.setup_routes(app)

    list_controller = ListController(app['list_repo'])
    list_controller.setup_routes(app)

    task_controller = TaskController(app['task_repo'])
    task_controller.setup_routes(app)

    user_controller = UserController(app['user_repo'])
    user_controller.setup_routes(app)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(allow_credentials=True, expose_headers="*", allow_headers="*")
    })

    for route in app.router.routes():
        cors.add(route)

    all_routes = '\n'
    for route in app.router.routes():
        all_routes += '{} {}> {}\n'.format(route.method, route.resource, route.handler.__qualname__)
    logger = logging.getLogger(__package__)
    logger.info(all_routes)

    # web.run_app(app, port=7070)
    # web.run_app(app)
    return app

