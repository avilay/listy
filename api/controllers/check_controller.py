import logging
from aiohttp import web


class CheckController:
    def setup_routes(self, app):
        app.router.add_route('GET', '/logs', self.check_logging)
        app.router.add_route('GET', '/error', self.check_error)
        app.router.add_route('GET', '/auth', self.check_auth)
        app.router.add_route('GET', '/hello', self.check_hello)

    async def check_hello(self, req):
        return web.Response(body=b"Hello, world")

    async def check_logging(self, req):
        logger = logging.getLogger(__package__)
        logger.debug('This is debug log')
        logger.info('This is info log')
        logger.warn('This is warning log')
        logger.error('This is error log')
        return web.Response(text='')

    async def check_error(self, req):
        raise RuntimeError('KABOOM!!')

    async def check_auth(self, req):
        return web.json_response(req['user'].to_dict())
