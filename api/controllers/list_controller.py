import logging
from aiohttp import web
import api.commons as comm


class ListController:
    def __init__(self, repo):
        self._logger = logging.getLogger(__package__)
        self._repo = repo

    def setup_routes(self, app):
        app.router.add_route('GET', '/lists', self.get_lists)
        app.router.add_route('GET', '/lists/{listid}', self.get_list)
        app.router.add_route('POST', '/lists', self.create_list)
        app.router.add_route('PATCH', '/lists/{listid}', self.update_list)
        app.router.add_route('DELETE', '/lists/{listid}', self.delete_list)

    async def get_lists(self, req):
        user = req['user']
        lsts = self._repo.get_lists(user_id=user.id)
        def_lst = lsts[0].to_dict()
        usergen_lsts = [lst.to_dict() for lst in lsts[1:]]
        all_lsts = dict(default_=def_lst, usergen=usergen_lsts)
        self._logger.debug('Returning {} lists'.format(len(all_lsts)))
        return web.json_response(data=all_lsts, dumps=comm.gen_json_dumper())

    async def get_list(self, req):
        user = req['user']
        list_id = req['listid']
        lst = self._repo.get_list(user_id=user.id, list_id=list_id)
        self._logger.debug('list id: {} name: {}'.format(lst.id, lst.name))
        return web.json_response(data=lst.to_dict(), dumps=comm.gen_json_dumper())

    async def create_list(self, req):
        user = req['user']
        post_params = await req.json()
        list_name = post_params.get('name', '')
        new_list = self._repo.create_list(user_id=user.id, list_name=list_name).to_dict()
        return web.json_response(data=new_list, dumps=comm.gen_json_dumper())

    async def update_list(self, req):
        user = req['user']
        list_id = req['listid']
        partial_list = await req.json()
        list_name = partial_list.get('name', '')
        list_state = partial_list.get('state', '')
        self._logger.debug('list name: {}  state: {}'.format(list_name, list_state))
        edited_list = self._repo.update_list(user_id=user.id, list_id=list_id, list_name=list_name, list_state=list_state)
        return web.json_response(data=edited_list.to_dict(), dumps=comm.gen_json_dumper())

    async def delete_list(self, req):
        user = req['user']
        list_id = req['listid']
        self._repo.delete_list(user.id, list_id)
        return web.HTTPNoContent()
