import logging
from datetime import timezone
import dateutil.parser as dtparser
from aiohttp import web
import api.commons as comm


class TaskController:
    def __init__(self, repo):
        self._logger = logging.getLogger(__package__)
        self._repo = repo

    def setup_routes(self, app):
        app.router.add_route('GET', '/scheduled-tasks', self.get_scheduled_tasks)

        app.router.add_route('GET', '/lists/{listid}/tasks', self.get_tasks)
        app.router.add_route('POST', '/lists/{listid}/tasks', self.create_task)

        app.router.add_route('GET', '/lists/{listid}/tasks/{taskid}', self.get_task_details)
        app.router.add_route('PATCH', '/lists/{listid}/tasks/{taskid}', self.update_task)
        app.router.add_route('DELETE', '/lists/{listid}/tasks/{taskid}', self.delete_task)

    async def get_scheduled_tasks(self, req):
        user = req['user']
        tasks = self._repo.get_scheduled_tasks(user.id)
        tasks = [task.to_dict() for task in tasks]
        self._logger.debug('Got {} tasks for user {}'.format(len(tasks), user))
        return web.json_response(data=tasks, dumps=comm.gen_json_dumper())

    async def get_tasks(self, req):
        user = req['user']
        listid = req['listid']
        filter_ = req.GET.get('filter')
        if filter_ is None:
            tasks = self._repo.get_tasks(user_id=user.id, list_id=listid)
        elif filter_ == 'recent':
            tasks = self._repo.get_recent_tasks(user_id=user.id, list_id=listid)
        else:
            raise web.HTTPBadRequest()
        tasks_dicts = [task.to_dict() for task in tasks]
        return web.json_response(data=tasks_dicts, dumps=comm.gen_json_dumper())

    async def get_task_details(self, req):
        user = req['user']
        listid = req['listid']
        taskid = req['taskid']
        task = self._repo.get_task_details(user_id=user.id, list_id=listid, task_id=taskid)
        task = task.to_dict()
        return web.json_response(data=task, dumps=comm.gen_json_dumper())

    def _extract_task_params(self, task):
        task_attrs = {}
        if 'title' in task:
            task_attrs['title'] = task['title']
        if 'state' in task:
            task_attrs['state'] = task['state']
        if 'scheduled_start_on' in task:
            if task['scheduled_start_on']:
                task_attrs['scheduled_start_on'] = dtparser.parse(task['scheduled_start_on']).astimezone(timezone.utc)
            else:
                task_attrs['scheduled_start_on'] = task['scheduled_start_on']
        if 'estimated_minutes' in task:
            task_attrs['estimated_minutes'] = task['estimated_minutes']
        if 'description' in task:
            task_attrs['description'] = task['description']
        return task_attrs

    async def create_task(self, req):
        user = req['user']
        listid = req['listid']
        post_params = await req.json()
        kwargs = dict(user_id=user.id, list_id=listid)
        task_attrs = self._extract_task_params(post_params)
        self._logger.debug('Creating following task')
        kwargs.update(task_attrs)
        self._logger.debug(kwargs)
        new_task = self._repo.create_task(**kwargs)
        new_task = new_task.to_dict()
        return web.json_response(data=new_task, dumps=comm.gen_json_dumper())

    async def delete_task(self, req):
        user = req['user']
        listid = req['listid']
        taskid = req['taskid']
        self._repo.delete_task(user.id, listid, taskid)
        return web.HTTPNoContent()

    async def update_task(self, req):
        user = req['user']
        list_id = req['listid']
        task_id = req['taskid']
        partial_task = await req.json()
        task_attrs = self._extract_task_params(partial_task)
        self._logger.debug('*****************')
        self._logger.debug(task_attrs)
        updated_task = self._repo.update_task(user.id, list_id, task_id, task_attrs)
        return web.json_response(data=updated_task.to_dict(), dumps=comm.gen_json_dumper())

