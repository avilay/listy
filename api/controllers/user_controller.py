import base64
import logging
from aiohttp import web
import api.commons as comm


class UserController:
    def __init__(self, repo):
        self._logger = logging.getLogger(__package__)
        self._repo = repo

    def setup_routes(self, app):
        app.router.add_route('POST', '/login', self.login)
        app.router.add_route('POST', '/register', self.register)
        app.router.add_route('GET', '/me', self.me)

    async def login(self, req):
        post_params = await req.json()
        email = post_params['email']
        password = post_params['password']
        user = self._repo.find_user(email, password)
        if user:
            user = user.to_dict()
            user['token'] = base64.b64encode('{}:{}'.format(email, password).encode('utf-8')).decode('utf-8')
            return web.json_response(data=user, dumps=comm.gen_json_dumper())
        else:
            raise web.HTTPUnauthorized()

    async def register(self, req):
        post_params = await req.json()
        name = post_params['name']
        email = post_params['email']
        password = post_params['password']
        user = self._repo.create_user(name, email, password)
        user = user.to_dict()
        user['token'] = base64.b64encode('{}:{}'.format(email, password).encode('utf-8')).decode('utf-8')
        return web.json_response(data=user, dumps=comm.gen_json_dumper())

    async def me(self, req):
        user = req['user']
        email = user.email
        password = req['password']
        user = user.to_dict()
        user['token'] = base64.b64encode('{}:{}'.format(email, password).encode('utf-8')).decode('utf-8')
        return web.json_response(data=user, dumps=comm.gen_json_dumper())
