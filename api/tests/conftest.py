import psycopg2 as pg
import pytest
from api.repository.pg import PgListRepository, PgTaskRepository, PgUserRepository
from .utils import *
from .data.data_generator import *


@pytest.fixture(scope='module')
def db(request):
    print('CONNECTING TO DB')
    db_ = pg.connect(**DBPARAMS)
    db_.autocommit = True
    request.addfinalizer(lambda: db_.close())
    return db_


@pytest.fixture(scope='module')
def userdb(request):
    pgrepo = PgUserRepository({'POSTGRES': DBPARAMS})
    request.addfinalizer(lambda: pgrepo.close())
    return pgrepo


@pytest.fixture(scope='module')
def taskdb(request):
    pgrepo = PgTaskRepository({'POSTGRES': DBPARAMS})
    request.addfinalizer(lambda: pgrepo.close())
    return pgrepo


@pytest.fixture(scope='module')
def listdb(request):
    pgrepo = PgListRepository({'POSTGRES': DBPARAMS})
    request.addfinalizer(lambda: pgrepo.close())
    return pgrepo


@pytest.fixture(scope='module')
def users():
    print('GENERATING USERS')
    db_ = pg.connect(**DBPARAMS)
    all_users = gen_users(db_)
    db_.close()
    return all_users


@pytest.fixture(scope='module')
def happy(users):
    print('GETTING HAPPY')
    for user in users:
        if user['email'] == 'happy@orange.com':
            return user
    return None


@pytest.fixture(scope='module')
def frozen(users):
    print('GETTING FROZEN')
    for user in users:
        if user['email'] == 'frozen@horizon.com':
            return user
    return None


@pytest.fixture(scope='module')
def cookie(users):
    print('GETTING COOKIE')
    for user in users:
        if user['email'] == 'cookie@monster.com':
            return user
    return None


@pytest.fixture
def lists():
    print('GENERATING LISTS')
    db_ = pg.connect(**DBPARAMS)
    all_lists = gen_lists(db_)
    db_.close()
    return all_lists

@pytest.fixture
def tasks():
    print('GENERATING TASKS')
    db_ = pg.connect(**DBPARAMS)
    all_tasks = gen_tasks(db_)
    db_.close()
    return all_tasks


class ListsFixture:
    def __init__(self):
        pass


@pytest.fixture
def happy_lists(happy, lists):
    print('GETTING HAPPY LISTS')
    def_list = None
    active_lists = []
    archived_lists = []
    for list_ in lists:
        if list_['user_id'] == happy['id']:
            if list_['name'] == '(Default)':
                def_list = list_
            elif list_['state'] == 'archived':
                archived_lists.append(list_)
            elif list_['state'] == 'active':
                active_lists.append(list_)
    return {
        'default_': def_list,
        'active': active_lists,
        'archived': archived_lists
    }


@pytest.fixture
def frozen_lists(frozen, lists):
    print('GETTING FROZEN LISTS')
    def_list = None
    active_lists = []
    archived_lists = []
    for list_ in lists:
        if list_['user_id'] == frozen['id']:
            if list_['name'] == '(Default)':
                def_list = list_
            elif list_['state'] == 'archived':
                archived_lists.append(list_)
            elif list_['state'] == 'active':
                active_lists.append(list_)
    return {
        'default_': def_list,
        'active': active_lists,
        'archived': archived_lists
    }


@pytest.fixture
def cookie_lists(cookie, lists):
    print('GETTING COOKIE LISTS')
    def_list = None
    active_lists = []
    archived_lists = []
    for list_ in lists:
        if list_['user_id'] == cookie['id']:
            if list_['name'] == '(Default)':
                def_list = list_
            elif list_['state'] == 'archived':
                archived_lists.append(list_)
            elif list_['state'] == 'active':
                active_lists.append(list_)
    return {
        'default_': def_list,
        'active': active_lists,
        'archived': archived_lists
    }


def attach_tasks_to_list(list_, all_tasks):
    list_['tasks'] = []
    for task in all_tasks:
        if task['list_id'] == list_['id']:
            task['list_name'] = list_['name']
            list_['tasks'].append(task)


def attach_tasks(user_lists, all_tasks):
    def_list = user_lists['default_']
    attach_tasks_to_list(def_list, all_tasks)

    for active_list in user_lists['active']:
        attach_tasks_to_list(active_list, all_tasks)

    for archived_list in user_lists['archived']:
        attach_tasks_to_list(archived_list, all_tasks)


@pytest.fixture
def happy_tasks(happy_lists, tasks):
    print('GETTING HAPPY TASKS')
    attach_tasks(happy_lists, tasks)
    return happy_lists


@pytest.fixture
def frozen_tasks(frozen_lists, tasks):
    print('GETTING FROZEN TASKS')
    attach_tasks(frozen_lists, tasks)
    return frozen_lists






