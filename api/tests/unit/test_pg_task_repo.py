import pytest
import shortuuid
import psycopg2.extras as pgextras
from avilabsutils.decorators import InvalidArgError

from api.repository.repo_exceptions import DataNotFoundError, NotEnoughArgsError
from ..utils import *

"""
+---------+
| FIXTURE | ==> dict
+---------+

+--------+
| PGREPO | ==> model
+--------+
"""
TASK_ATTRS = ['id', 'title', 'state', 'created_on', 'minutes_spent', 'scheduled_start_on', 'estimated_minutes', 'list_id', 'list_name']
TASK_DETAILS_ATTRS = TASK_ATTRS + ['description', 'last_start_on', 'completed_on']


def _flatten(user_tasks, default=True, active=True, archived=True):
    all_tasks = []
    if default:
        all_tasks += [t for t in user_tasks['default_']['tasks']]
    if active:
        all_tasks += [t for active_list in user_tasks['active'] for t in active_list['tasks']]
    if archived:
        all_tasks += [t for archived_list in user_tasks['archived'] for t in archived_list['tasks']]
    return all_tasks


def _get_task(db, task_id):
    cur = db.cursor(cursor_factory=pgextras.RealDictCursor)
    sql = '''
        SELECT t.id, t.title, t.state, t.created_on, t.minutes_spent, t.scheduled_start_on, t.estimated_minutes, t.description, t.last_start_on, t.completed_on, t.list_id, l.name as list_name
        FROM tasks t
        INNER JOIN lists l ON t.list_id = l.id
        WHERE t.id = %s
        '''
    cur.execute(sql, [task_id])
    task = cur.fetchone()
    cur.close()
    return task


# Happy day scenario - user gets tasks from a non-empty default list
#                    - user gets tasks from a non-empty non-default list
@pytest.mark.get
def test_get_tasks(taskdb, happy, happy_tasks):
    def_list = happy_tasks['default_']
    exp_def_tasks = def_list['tasks']
    act_def_tasks = taskdb.get_tasks(happy['id'], def_list['id'])
    act_def_tasks = [task.to_dict() for task in act_def_tasks]
    assert_unordered_arrays(exp_def_tasks, act_def_tasks, TASK_ATTRS)

    nondef_list = happy_tasks['active'][0]
    exp_nondef_tasks = nondef_list['tasks']
    act_nondef_tasks = taskdb.get_tasks(happy['id'], nondef_list['id'])
    act_nondef_tasks = [task.to_dict() for task in act_nondef_tasks]
    assert_unordered_arrays(exp_nondef_tasks, act_nondef_tasks, TASK_ATTRS)


# Happy day scenario - user gets tasks from an empty default list
#                    - user gets tasks from an empty non-default list
@pytest.mark.get
def test_get_tasks_empty(taskdb, cookie, cookie_lists):
    def_list = cookie_lists['default_']
    act_def_tasks = taskdb.get_tasks(cookie['id'], def_list['id'])
    assert [] == act_def_tasks

    nondef_list = cookie_lists['active'][0]
    act_nondef_tasks = taskdb.get_tasks(cookie['id'], nondef_list['id'])
    assert [] == act_nondef_tasks


# User attempts to get tasks from a list they do not own
@pytest.mark.get
def test_get_other_tasks(taskdb, happy, frozen_lists):
    other_def_list = frozen_lists['default_']
    with pytest.raises(DataNotFoundError):
        taskdb.get_tasks(happy['id'], other_def_list['id'])

    other_nondef_list = frozen_lists['active'][0]
    with pytest.raises(DataNotFoundError):
        taskdb.get_tasks(happy['id'], other_nondef_list['id'])


# User attempts to get tasks from a list that does not exist in the db
@pytest.mark.get
def test_get_tasks_absent(taskdb, happy):
    with pytest.raises(DataNotFoundError):
        taskdb.get_tasks(happy['id'], 1000)


# User attempts to get tasks from a list with a non-numeric id
@pytest.mark.get
def test_get_tasks_bad(taskdb):
    with pytest.raises(InvalidArgError):
        taskdb.get_tasks(1, 'hahaha')

    with pytest.raises(InvalidArgError):
        taskdb.get_tasks('hahaha', 1)


# Invalid user
@pytest.mark.get
def test_get_tasks_bad_user(taskdb):
    with pytest.raises(DataNotFoundError):
        taskdb.get_tasks(1000, 1)


@pytest.mark.get
def test_get_recent_tasks(taskdb, happy, happy_tasks):
    now = datetime.now(timezone.utc)
    list_ = happy_tasks['active'][0]
    tasks = list_['tasks']
    exp_tasks = []
    for task in tasks:
        if task['state'] != 'done':
            exp_tasks.append(task)
        else:
            if (now - task['completed_on']).total_seconds() <= 3 * 24 * 60 * 60:
                exp_tasks.append(task)
    act_tasks = taskdb.get_recent_tasks(happy['id'], list_['id'])
    act_tasks = [act_task.to_dict() for act_task in act_tasks]
    assert_unordered_arrays(exp_tasks, act_tasks, TASK_ATTRS)


# Happy day - user getting their own task from their own list
@pytest.mark.get
def test_get_task_details(taskdb, happy, happy_tasks):
    active_list = happy_tasks['active'][0]
    exp_task = active_list['tasks'][0]
    act_task = taskdb.get_task_details(happy['id'], active_list['id'], exp_task['id']).to_dict()
    assert exp_task == act_task


# User trying to get a task that exists in somebody else's list
@pytest.mark.get
def test_get_task_details_other(taskdb, happy, frozen_tasks):
    def_list = frozen_tasks['default_']
    with pytest.raises(DataNotFoundError):
        taskdb.get_task_details(happy['id'], def_list['id'], def_list['tasks'][0]['id'])


# User trying to get a non-existant task from a list that has other tasks
@pytest.mark.get
def test_get_task_details_absent(taskdb, happy, happy_lists):
    def_list = happy_lists['default_']
    with pytest.raises(DataNotFoundError):
        taskdb.get_task_details(happy['id'], def_list['id'], 1000)


# User trying to get a task from an empty list
@pytest.mark.get
def test_get_task_details_empty(taskdb, cookie, cookie_lists):
    def_list = cookie_lists['default_']
    with pytest.raises(DataNotFoundError):
        taskdb.get_task_details(cookie['id'], def_list['id'], 1)


# Happy day - user creates a task in a list they own
@pytest.mark.create
def test_create_task(db, taskdb, cookie, cookie_lists):
    now = datetime.now(timezone.utc)

    title = 'new task ' + shortuuid.uuid()
    def_list = cookie_lists['default_']
    new_task = taskdb.create_task(
        user_id=cookie['id'],
        list_id=def_list['id'],
        title=title)

    cur = db.cursor()
    cur.execute('SELECT COUNT(*) FROM tasks WHERE list_id = %s', [def_list['id']])
    task_count = cur.fetchone()[0]
    cur.close()
    assert 1 == task_count

    assert 0 < new_task.id
    assert title == new_task.title
    assert 'not-started' == new_task.state
    assert now < new_task.created_on
    assert 0 == new_task.minutes_spent
    assert def_list['id'] == new_task.list_id
    assert def_list['name'] == new_task.list_name

    title = 'new task ' + shortuuid.uuid()
    eta = 10
    desc = 'new task desc'
    new_task = taskdb.create_task(
        user_id=cookie['id'],
        list_id=def_list['id'],
        title=title,
        estimated_minutes=eta,
        description=desc
    )
    assert 0 < new_task.id
    assert title == new_task.title
    assert eta == new_task.estimated_minutes
    assert desc == new_task.description


# User tries to create a list with an empty title or negative estimated mins
@pytest.mark.create
def test_create_task_bad(taskdb, cookie, cookie_lists):
    def_list = cookie_lists['default_']
    with pytest.raises(InvalidArgError):
        taskdb.create_task(
            user_id=cookie['id'],
            list_id=def_list['id'],
            title='',
            estimated_minutes=10
        )

    with pytest.raises(InvalidArgError):
        taskdb.create_task(
            user_id=cookie['id'],
            list_id=def_list['id'],
            title='some title',
            estimated_minutes=-10
        )


# Happy day - user deletes task from own list
@pytest.mark.delete
def test_delete_task(db, taskdb, happy, happy_tasks):
    def_list = happy_tasks['default_']
    task_tbd = def_list['tasks'][0]
    taskdb.delete_task(happy['id'], def_list['id'], task_tbd['id'])
    cur = db.cursor()
    cur.execute('SELECT * FROM tasks WHERE id = %s', [task_tbd['id']])
    assert 0 == cur.rowcount


# User tries to delete task from somebody else's list
@pytest.mark.delete
def test_delete_task_other(taskdb, happy, frozen_tasks):
    def_list = frozen_tasks['default_']
    task_tbd = def_list['tasks'][0]
    with pytest.raises(DataNotFoundError):
        taskdb.delete_task(happy['id'], def_list['id'], task_tbd['id'])


# User tries to delete non existing task from own list
@pytest.mark.delete
def test_delete_task_absent(taskdb, happy, happy_lists):
    active_list = happy_lists['active'][0]
    with pytest.raises(DataNotFoundError):
        taskdb.delete_task(happy['id'], active_list['id'], 1000)


@pytest.mark.scheduled
def test_get_scheduled_tasks(taskdb, happy, happy_tasks):
    exp_tasks = []
    for task in happy_tasks['default_']['tasks']:
        if task['state'] in ['scheduled', 'in-progress', 'paused']:
            exp_tasks.append(task)
    for active_list in happy_tasks['active']:
        for task in active_list['tasks']:
            if task['state'] in ['scheduled', 'in-progress', 'paused']:
                exp_tasks.append(task)
    act_tasks = taskdb.get_scheduled_tasks(happy['id'])
    print(len(exp_tasks), len(act_tasks))
    act_tasks = [act_task.to_dict() for act_task in act_tasks]
    assert_unordered_arrays(exp_tasks, act_tasks, TASK_ATTRS)


@pytest.mark.scheduled
def test_get_scheduled_tasks_empty(taskdb, cookie):
    act_tasks = taskdb.get_scheduled_tasks(cookie['id'])
    assert [] == act_tasks


@pytest.mark.update
def test_update_remove_attrs(db, taskdb, happy, happy_tasks):
    tasks = _flatten(happy_tasks, archived=False)

    exp_task = list(filter(lambda t: t['description'], tasks))[0]
    exp_task['description'] = None
    updated_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs={
            'description': None
        }
    )
    act_task = updated_task.to_dict()
    assert exp_task == act_task

    exp_task = list(filter(lambda t: t['scheduled_start_on'], tasks))[0]
    exp_task['scheduled_start_on'] = None
    updated_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs={
            'scheduled_start_on': None
        }
    )
    act_task = updated_task.to_dict()
    assert exp_task == act_task

    exp_task = list(filter(lambda t: t['estimated_minutes'] > 0, tasks))[0]
    exp_task['estimated_minutes'] = None
    updated_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs={
            'estimated_minutes': None
        }
    )
    act_task = updated_task.to_dict()
    assert exp_task == act_task


@pytest.mark.update
def test_update_task(db, taskdb, happy, happy_tasks):
    # Happy day - change title, estimated_minutes, description
    tasks = _flatten(happy_tasks, archived=False)
    exp_task = list(filter(lambda t: not t['description'], tasks))[0]
    exp_task['title'] = 'new title ' + shortuuid.uuid()
    exp_task['estimated_minutes'] = 100
    exp_task['description'] = 'new description here'
    updated_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(
            title=exp_task['title'],
            estimated_minutes=exp_task['estimated_minutes'],
            description=exp_task['description']
        )
    )

    act_task = updated_task.to_dict()
    assert exp_task == act_task

    act_task = _get_task(db, exp_task['id'])
    assert exp_task == act_task

    # Try to set title to ''
    with pytest.raises(InvalidArgError):
        taskdb.update_task(
            user_id=happy['id'],
            list_id=exp_task['list_id'],
            task_id=exp_task['id'],
            task_attrs=dict(title='')
        )

    # Don't set anything
    with pytest.raises(InvalidArgError):
        taskdb.update_task(
            user_id=happy['id'],
            list_id=exp_task['list_id'],
            task_id=exp_task['id'],
            task_attrs={}
        )


@pytest.mark.update
def test_update_task_scheduled_state(db, taskdb, happy, happy_tasks):
    now = datetime.now(timezone.utc)
    tasks = _flatten(happy_tasks, archived=False)
    unscheduled_tasks = list(filter(lambda t: not t['scheduled_start_on'], tasks))

    # scheduling a task should change its state to scheduled
    exp_task = unscheduled_tasks[0]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(scheduled_start_on=now)
    )
    assert 'scheduled' == act_task.state
    assert now == act_task.scheduled_start_on

    act_task = _get_task(db, exp_task['id'])
    assert 'scheduled' == act_task['state']
    assert now == act_task['scheduled_start_on']

    # Cannot set state while setting scheduled_start_on
    exp_task = unscheduled_tasks[1]
    with pytest.raises(InvalidArgError):
        taskdb.update_task(
            user_id=happy['id'],
            list_id=exp_task['list_id'],
            task_id=exp_task['id'],
            task_attrs=dict(
                scheduled_start_on=now,
                state='paused'
            )
        )

    # scheduled tasks can be started or completed
    scheduled_tasks = list(filter(lambda t: t['scheduled_start_on'], tasks))
    exp_task = scheduled_tasks[0]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(state='in-progress')
    )
    assert 'in-progress' == act_task.state
    assert now < act_task.last_start_on
    assert act_task.completed_on is None

    exp_task = scheduled_tasks[1]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(state='done')
    )
    assert 'done' == act_task.state
    assert act_task.last_start_on is None
    assert now < act_task.completed_on


# not-started tasks can be directly marked as done
@pytest.mark.update
def test_update_task_notstarted_state(taskdb, happy, happy_tasks):
    now = datetime.now(timezone.utc)
    tasks = _flatten(happy_tasks, archived=False)
    ns_tasks = list(filter(lambda x: x['state'] == 'not-started', tasks))

    exp_task = ns_tasks[0]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(state='done')
    )
    assert 'done' == act_task.state
    assert now < act_task.completed_on
    assert 0 == act_task.minutes_spent


# in-progress tasks can be completed or paused
@pytest.mark.update
def test_update_task_inprogress_state(taskdb, happy, happy_tasks):
    now = datetime.now(timezone.utc)
    tasks = _flatten(happy_tasks, archived=False)
    ip_tasks = list(filter(lambda x: x['state'] == 'in-progress', tasks))

    exp_task = ip_tasks[0]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(state='done')
    )
    assert 'done' == act_task.state
    assert exp_task['minutes_spent'] < act_task.minutes_spent
    assert now < act_task.completed_on

    exp_task = ip_tasks[1]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(state='paused')
    )
    assert 'paused' == act_task.state
    assert exp_task['minutes_spent'] < act_task.minutes_spent
    assert act_task.completed_on is None


# paused tasks can be restarted or marked as completed
@pytest.mark.update
def test_update_task_paused_state(taskdb, happy, happy_tasks):
    now = datetime.now(timezone.utc)
    tasks = _flatten(happy_tasks, archived=False)
    paused_tasks = list(filter(lambda x: x['state'] == 'paused', tasks))

    exp_task = paused_tasks[0]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(state='in-progress')
    )
    assert 'in-progress' == act_task.state
    assert now < act_task.last_start_on

    exp_task = paused_tasks[1]
    act_task = taskdb.update_task(
        user_id=happy['id'],
        list_id=exp_task['list_id'],
        task_id=exp_task['id'],
        task_attrs=dict(state='done')
    )
    assert 'done' == act_task.state
    assert now < act_task.completed_on


@pytest.mark.update
def test_update_task_bad_states(taskdb, happy, happy_tasks):
    tasks = _flatten(happy_tasks, archived=False)

    # not-started task cannot go to scheduled (directly), in-progress, or paused
    ns_task = list(filter(lambda x: x['state'] == 'not-started', tasks))[0]
    for state in ['scheduled', 'in-progress', 'paused']:
        with pytest.raises(InvalidArgError):
            taskdb.update_task(
                user_id=happy['id'],
                list_id=ns_task['list_id'],
                task_id=ns_task['id'],
                task_attrs=dict(state=state)
            )

    # scheduled task cannot go to not-started or paused
    sd_task = list(filter(lambda x: x['state'] == 'scheduled', tasks))[0]
    for state in ['not-started', 'paused']:
        with pytest.raises(InvalidArgError):
            taskdb.update_task(
                user_id=happy['id'],
                list_id=sd_task['list_id'],
                task_id=sd_task['id'],
                task_attrs=dict(state=state)
            )

    # in-progress task cannot go to not-started or scheduled
    ip_task = list(filter(lambda x: x['state'] == 'in-progress', tasks))[0]
    for state in ['not-started', 'scheduled']:
        with pytest.raises(InvalidArgError):
            taskdb.update_task(
                user_id=happy['id'],
                list_id=ip_task['list_id'],
                task_id=ip_task['id'],
                task_attrs=dict(state=state)
            )

    # paused task cannot go to not-started or scheduled
    p_task = list(filter(lambda x: x['state'] == 'paused', tasks))[0]
    for state in ['not-started', 'scheduled']:
        with pytest.raises(InvalidArgError):
            taskdb.update_task(
                user_id=happy['id'],
                list_id=p_task['list_id'],
                task_id=p_task['id'],
                task_attrs=dict(state=state)
            )

    # done task cannot go to any state, i.e., not-started, scheduled, in-progress, paused
    d_task = list(filter(lambda x: x['state'] == 'done', tasks))[0]
    for state in ['not-stareted', 'scheduled', 'in-progress', 'paused']:
        with pytest.raises(InvalidArgError):
            taskdb.update_task(
                user_id=happy['id'],
                list_id=d_task['list_id'],
                task_id=d_task['id'],
                task_attrs=dict(state=state)
            )
