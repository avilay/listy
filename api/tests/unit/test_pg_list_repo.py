import pytest
import shortuuid
import psycopg2.extras as pgextras
from avilabsutils.decorators import InvalidArgError

from api.repository.repo_exceptions import DataNotFoundError, NotEnoughArgsError
from ..utils import *

"""
+---------+
| FIXTURE | ==> dict
+---------+

+--------+
| PGREPO | ==> model
+--------+
"""


def list_from_db(db, listid):
    cur = db.cursor(cursor_factory=pgextras.RealDictCursor)
    cur.execute('SELECT * FROM lists WHERE id = %s', [listid])
    list_ = cur.fetchone()
    cur.close()
    return list_


def test_get_lists(listdb, happy, happy_lists):
    exp_lists = [happy_lists['default_']]
    exp_lists += happy_lists['active']
    exp_lists += happy_lists['archived']
    act_lists = listdb.get_lists(happy['id'])
    act_lists = [act_list.to_dict() for act_list in act_lists]
    assert_unordered_arrays(exp_lists, act_lists)

    with pytest.raises(InvalidArgError):
        listdb.get_lists(-1)

    with pytest.raises(InvalidArgError):
        listdb.get_lists('hahaha')

    with pytest.raises(DataNotFoundError):
        listdb.get_lists(1000)


def test_create_list(db, listdb, happy):
    now = datetime.now(timezone.utc)
    name = shortuuid.uuid()
    new_list = listdb.create_list(user_id=happy['id'], list_name=name)
    new_list = new_list.to_dict()
    assert name == new_list['name']
    assert 'active' == new_list['state']
    assert now < new_list['created_on']
    assert now < new_list['updated_on']
    assert happy['id'] == new_list['user_id']

    new_db_list = list_from_db(db, new_list['id'])
    assert new_db_list == new_list

    with pytest.raises(InvalidArgError):
        listdb.create_list(user_id=happy['id'], list_name='(Default)')

    with pytest.raises(InvalidArgError):
        listdb.create_list(user_id=happy['id'], list_name='')

    with pytest.raises(InvalidArgError):
        listdb.create_list(user_id=-1, list_name='some name')

    with pytest.raises(DataNotFoundError):
        listdb.create_list(user_id=1000, list_name='some name')

    with pytest.raises(InvalidArgError):
        listdb.create_list(user_id='hahaha', list_name='some name')


def test_update_list(db, listdb, happy, happy_lists, frozen_lists):
    now = datetime.now(timezone.utc)

    # Happy day - change name and state of an active list
    happy_active_list = happy_lists['active'][0]
    name = shortuuid.uuid()
    updated_list = listdb.update_list(user_id=happy['id'], list_id=happy_active_list['id'], list_name=name, list_state='archived')
    updated_list = updated_list.to_dict()
    assert name == updated_list['name']
    assert 'archived' == updated_list['state']
    assert happy_active_list['created_on'] == updated_list['created_on']
    assert now < updated_list['updated_on']
    assert happy['id'] == updated_list['user_id']
    updated_db_list = list_from_db(db, updated_list['id'])
    assert updated_db_list == updated_list

    # Happy day - change an archived list back to active
    happy_archived_list = happy_lists['archived'][0]
    updated_list = listdb.update_list(user_id=happy['id'], list_id=happy_archived_list['id'], list_state='active')
    updated_list = updated_list.to_dict()
    assert 'active' == updated_list['state']
    updated_db_list = list_from_db(db, updated_list['id'])
    assert updated_db_list == updated_list

    # Change the name to (Default)
    with pytest.raises(InvalidArgError):
        listdb.update_list(user_id=happy['id'], list_id=happy_active_list['id'], list_name='(Default)')

    # Change the name to blank
    with pytest.raises(NotEnoughArgsError):
        listdb.update_list(user_id=happy['id'], list_id=happy_active_list['id'], list_name='')

    # Change the state to an invalid state
    with pytest.raises(InvalidArgError):
        listdb.update_list(user_id=happy['id'], list_id=happy_active_list['id'], list_state='hahaha')

    # Pass in an invalid user id
    with pytest.raises(DataNotFoundError):
        listdb.update_list(user_id=1000, list_id=happy_active_list['id'], list_name='some name')

    # Pass in an invalid list id
    with pytest.raises(DataNotFoundError):
        listdb.update_list(user_id=happy['id'], list_id=1000, list_name='some name')

    # Change the name of the default list
    def_list = happy_lists['default_']
    with pytest.raises(DataNotFoundError):
        listdb.update_list(user_id=happy['id'], list_id=def_list['id'], list_name='some name')

    # Change name of somebody else's list
    frozen_active_list = frozen_lists['active'][0]
    with pytest.raises(DataNotFoundError):
        listdb.update_list(user_id=happy['id'], list_id=frozen_active_list['id'], list_name='some name')


def test_delete_list(db, listdb, happy, happy_lists, frozen_lists):
    # Happy day - delete an active list
    active_list = happy_lists['active'][0]
    listdb.delete_list(happy['id'], active_list['id'])
    assert list_from_db(db, active_list['id']) is None

    # Happy day - delete an archived list
    archived_list = happy_lists['archived'][0]
    listdb.delete_list(happy['id'], archived_list['id'])
    assert list_from_db(db, archived_list['id']) is None

    # Try to delete (Default) list
    def_list = happy_lists['default_']
    with pytest.raises(DataNotFoundError):
        listdb.delete_list(happy['id'], def_list['id'])

    # Try to delete somebody else's list
    frozen_active_list = frozen_lists['active'][0]
    with pytest.raises(DataNotFoundError):
        listdb.delete_list(happy['id'], frozen_active_list['id'])

    # Pass in invalid user id
    with pytest.raises(DataNotFoundError):
        listdb.delete_list(1000, active_list['id'])

    # Pass in invalid list id
    with pytest.raises(DataNotFoundError):
        listdb.delete_list(happy['id'], 1000)
