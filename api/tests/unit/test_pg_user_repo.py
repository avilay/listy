from datetime import datetime, timezone
import psycopg2.extras as pgextras
import shortuuid

"""
+---------+
| FIXTURE | ==> dict
+---------+

+--------+
| PGREPO | ==> model
+--------+
"""


def test_create_user(db, userdb):
    now = datetime.now(timezone.utc)
    cur = db.cursor(cursor_factory=pgextras.RealDictCursor)
    cur.execute('SELECT COUNT(*) AS num_users FROM users')
    num_users_before = cur.fetchone()['num_users']

    name = 'User name ' + shortuuid.uuid()
    email = shortuuid.uuid() + '@avilaylabs.net'
    password = 'secret'
    user = userdb.create_user(name=name, email=email, password=password)

    cur.execute('SELECT COUNT(*) AS num_users FROM users')
    num_users_after = cur.fetchone()['num_users']
    assert num_users_after == num_users_before + 1

    assert name == user.name
    assert email == user.email
    assert user.check_password(password)
    assert now < user.created_on

    cur.execute('SELECT * FROM lists WHERE user_id = %s', [user.id])
    def_list = cur.fetchone()
    assert '(Default)' == def_list['name']
    assert now < def_list['created_on']
    assert now < def_list['updated_on']

    cur.close()
