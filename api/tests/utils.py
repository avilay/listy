import os.path as path
from datetime import datetime, timezone, timedelta

DBPARAMS = {
    'host': 'localhost',
    'database': 'listy',
    'user': 'avilay.parekh'
}

WEBHOST = 'localhost'
PORT = '7070'


LISTS_URL = 'http://' + WEBHOST + ':' + PORT + '/lists'
LIST_URL = 'http://' + WEBHOST + ':' + PORT + '/lists/{listid}'

TASKS_URL = 'http://' + WEBHOST + ':' + PORT + '/lists/{listid}/tasks'
TASK_URL = 'http://' + WEBHOST + ':' + PORT + '/lists/{listid}/tasks/{taskid}'
SCHEDULED_TASKS_URL = 'http://' + WEBHOST + ':' + PORT + '/scheduled-tasks'

JSON_HEADERS = {
    'Accepts': 'application/json',
    'Content-Type': 'application/json'
}


def assert_unordered_arrays(exps, acts, attrs=None):
    assert len(exps) == len(acts)
    if attrs:
        exps = [{attr: exp[attr] for attr in attrs} for exp in exps]
        acts = [{attr: act[attr] for attr in attrs} for act in acts]
    for exp in exps:
        try:
            assert exp in acts
        except AssertionError as ae:
            raise AssertionError('Cound not find {} in actual array'.format(exp)) from ae


def to_json(d):
    for key in d:
        if isinstance(d[key], datetime):
            d[key] = d[key].astimezone(timezone.utc).isoformat()
    return d
