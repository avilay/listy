import requests
import dateutil.parser as dtparser
import json

import shortuuid

from api.tests.utils import *

"""
+-----+
| API | ==> json
+-----+

+---------+
| FIXTURE | ==> dict
+---------+
"""

TASK_ATTRS = ['id', 'title', 'state', 'created_on', 'minutes_spent', 'scheduled_start_on', 'estimated_minutes', 'list_id', 'list_name']
TASK_DETAILS_ATTRS = TASK_ATTRS + ['description', 'last_start_on', 'completed_on']


# Happy day - user gets all tasks from a non-empty default list
#           - user gets all tasks from a non-empty active list
#           - user gets all tasks from a non-empty archived list
def test_get_tasks(happy, happy_tasks):
    auth = (happy['email'], happy['password'])

    def_list = happy_tasks['default_']
    exp_def_tasks = def_list['tasks']
    exp_def_tasks = [to_json(t) for t in exp_def_tasks]

    url = TASKS_URL.format(listid=def_list['id'])
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_tasks = resp.json()

    assert_unordered_arrays(exp_def_tasks, act_tasks, TASK_ATTRS)

    active_list = happy_tasks['active'][0]
    exp_active_tasks = active_list['tasks']
    exp_active_tasks = [to_json(t) for t in exp_active_tasks]

    url = TASKS_URL.format(listid=active_list['id'])
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_tasks = resp.json()

    assert_unordered_arrays(exp_active_tasks, act_tasks, TASK_ATTRS)

    archived_list = happy_tasks['archived'][0]
    exp_archived_tasks = archived_list['tasks']
    exp_archived_tasks = [to_json(t) for t in exp_archived_tasks]

    url = TASKS_URL.format(listid=archived_list['id'])
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_tasks = resp.json()

    assert_unordered_arrays(exp_archived_tasks, act_tasks, TASK_ATTRS)


# User gets empty list of tasks from an empty default list
#                               from an empty active list
def test_get_tasks_empty(cookie, cookie_lists):
    auth = (cookie['email'], cookie['password'])

    def_list = cookie_lists['default_']
    url = TASKS_URL.format(listid=def_list['id'])
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_tasks = resp.json()
    assert [] == act_tasks

    active_list = cookie_lists['active'][0]
    url = TASKS_URL.format(listid=active_list['id'])
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_tasks = resp.json()
    assert [] == act_tasks


# User tries to get tasks from a list that does not belong to them
def test_get_other_tasks(happy, frozen_lists):
    auth = (happy['email'], happy['password'])
    other_def_list = frozen_lists['default_']
    url = TASKS_URL.format(listid=other_def_list['id'])
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 403 == resp.status_code


# User tries to get tasks from a non-existant list
def test_get_tasks_absent(happy):
    auth = (happy['email'], happy['password'])
    url = TASKS_URL.format(listid=1000)
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 403 == resp.status_code


# User provides a non-numeric list id
def test_get_tasks_bad(happy):
    auth = (happy['email'], happy['password'])
    url = TASKS_URL.format(listid='hahaha')
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 400 == resp.status_code


def test_get_recent_tasks(happy, happy_tasks):
    now = datetime.now(timezone.utc)
    auth = (happy['email'], happy['password'])
    active_list = happy_tasks['active'][0]
    exp_tasks = []
    for task in active_list['tasks']:
        if task['state'] != 'done':
            exp_tasks.append(to_json(task))
        else:
            if (now - task['completed_on']).total_seconds() <= 3 * 24 * 60 * 60:
                exp_tasks.append(to_json(task))
    url = TASKS_URL.format(listid=active_list['id'])
    url += '?filter=recent'
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_tasks = resp.json()
    assert_unordered_arrays(exp_tasks, act_tasks, TASK_ATTRS)


def test_get_recent_tasks_bad(happy, happy_tasks):
    auth = (happy['email'], happy['password'])
    url = TASKS_URL.format(listid=happy_tasks['active'][0]['id'])
    url += '?filter=hahaha'
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 400 == resp.status_code


def test_get_task_details(happy, happy_tasks):
    def_list = happy_tasks['default_']
    exp_task = def_list['tasks'][0]
    exp_task = to_json(exp_task)
    auth = (happy['email'], happy['password'])
    url = TASK_URL.format(listid=def_list['id'], taskid=exp_task['id'])
    resp = requests.get(url, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_task = resp.json()
    assert exp_task == act_task


def test_create_task(cookie, cookie_lists):
    now = datetime.now(timezone.utc)

    def_list = cookie_lists['default_']
    params = {
        'title': 'new title' + shortuuid.uuid(),
        'eta': 10,
        'desc': 'new task desc',
    }
    auth = (cookie['email'], cookie['password'])
    url = TASKS_URL.format(listid=def_list['id'])
    resp = requests.post(url, headers=JSON_HEADERS, auth=auth, data=json.dumps(params))
    assert 200 == resp.status_code
    new_task = resp.json()
    assert 0 < new_task['id']
    assert params['title'] == new_task['title']
    assert 'not-started' == new_task['state']
    assert now < dtparser.parse(new_task['created_on'])
    assert 0 == new_task['minutes_spent']
    assert def_list['id'] == new_task['list_id']
    assert def_list['name'] == new_task['list_name']


def test_delete_task(happy, happy_tasks):
    auth = (happy['email'], happy['password'])
    active_list = happy_tasks['active'][0]
    task_tbd = active_list['tasks'][0]
    url = TASK_URL.format(listid=active_list['id'], taskid=task_tbd['id'])
    resp = requests.delete(url, auth=auth)
    assert 204 == resp.status_code


def test_delete_task_other(happy, frozen_tasks):
    auth = (happy['email'], happy['password'])
    def_list = frozen_tasks['default_']
    task_tbd = def_list['tasks'][0]
    url = TASK_URL.format(listid=def_list['id'], taskid=task_tbd['id'])
    resp = requests.delete(url, auth=auth)
    assert 403 == resp.status_code


def test_get_scheduled_tasks(happy, happy_tasks):
    exp_tasks = []
    for task in happy_tasks['default_']['tasks']:
        if task['state'] in ['scheduled', 'in-progress', 'paused']:
            exp_tasks.append(task)
    for active_list in happy_tasks['active']:
        for task in active_list['tasks']:
            if task['state'] in ['scheduled', 'in-progress', 'paused']:
                exp_tasks.append(task)
    exp_tasks = [to_json(t) for t in exp_tasks]

    auth = (happy['email'], happy['password'])
    resp = requests.get(SCHEDULED_TASKS_URL, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    act_tasks = resp.json()
    assert_unordered_arrays(exp_tasks, act_tasks, TASK_ATTRS)
















