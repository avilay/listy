import requests
import dateutil.parser as dtparser
import json

import shortuuid

from api.tests.utils import *

"""
+-----+
| API | -- json -->  o
+-----+

+---------+
| FIXTURE | -- dict --> o
+---------+
"""


def test_get_lists(happy, happy_lists):
    exp_usergen_lists = happy_lists['active'] + happy_lists['archived']
    exp_usergen_lists = [to_json(l) for l in exp_usergen_lists]
    exp_default_list = to_json(happy_lists['default_'])
    auth = (happy['email'], happy['password'])
    resp = requests.get(LISTS_URL, headers=JSON_HEADERS, auth=auth)
    assert 200 == resp.status_code
    resp_lists = resp.json()
    act_usergen_lists = resp_lists['usergen']
    act_default_list = resp_lists['default_']
    assert_unordered_arrays(exp_usergen_lists, act_usergen_lists)
    assert exp_default_list == act_default_list


def test_create_list(happy):
    # Happy day scenario - valid user tries to create a list with a valid list name
    now = datetime.now(timezone.utc)
    auth = (happy['email'], happy['password'])
    params = {"name": "a new test list"}
    resp = requests.post(LISTS_URL, headers=JSON_HEADERS, auth=auth, data=json.dumps(params))
    assert 200 == resp.status_code
    new_list = resp.json()
    assert new_list['id'] > 0
    assert params['name'] == new_list['name']
    assert 'active' == new_list['state']
    assert now < dtparser.parse(new_list['created_on'])
    assert now < dtparser.parse(new_list['updated_on'])
    assert happy['id'] == new_list['user_id']

    # Create lists with bad names
    auth = (happy['email'], happy['password'])
    params = [{'name': '(Default)'}, {'name': ''}]
    for param in params:
        resp = requests.post(LISTS_URL, headers=JSON_HEADERS, auth=auth, data=json.dumps(param))
        assert 400 == resp.status_code


# Happy day - valid user edits the name of a non-default list
#           - valid user edits the state of a non-default list
#           - valid user edits both the name and the state
def test_edit_list_for_valid_user(happy, happy_lists):
    own_list = happy_lists['active'][0]
    now = datetime.now(timezone.utc)
    auth = (happy['email'], happy['password'])
    url = LIST_URL.format(listid=own_list['id'])
    new_name = shortuuid.uuid()

    params = {"name": new_name}
    resp = requests.patch(url, headers=JSON_HEADERS, auth=auth, data=json.dumps(params))
    assert 200 == resp.status_code
    edited_list = resp.json()
    assert own_list['id'] == edited_list['id']
    assert new_name == edited_list['name']
    assert own_list['state'] == edited_list['state']
    assert own_list['created_on'] == dtparser.parse(edited_list['created_on'])
    assert now < dtparser.parse(edited_list['updated_on'])
    assert own_list['user_id'] == edited_list['user_id']

    now = datetime.now(timezone.utc)
    params = {"state": "archived"}
    resp = requests.patch(url, headers=JSON_HEADERS, auth=auth, data=json.dumps(params))
    assert 200 == resp.status_code
    edited_list = resp.json()
    assert own_list['id'] == edited_list['id']
    assert new_name == edited_list['name']
    assert 'archived' == edited_list['state']
    assert own_list['created_on'] == dtparser.parse(edited_list['created_on'])
    assert now < dtparser.parse(edited_list['updated_on'])
    assert own_list['user_id'] == edited_list['user_id']

    now = datetime.now(timezone.utc)
    params = {"name": shortuuid.uuid(), "state": "active"}
    resp = requests.patch(url, headers=JSON_HEADERS, auth=auth, data=json.dumps(params))
    assert 200 == resp.status_code
    edited_list = resp.json()
    assert own_list['id'] == edited_list['id']
    assert params['name'] == edited_list['name']
    assert params['state'] == edited_list['state']
    assert own_list['created_on'] == dtparser.parse(edited_list['created_on'])
    assert now < dtparser.parse(edited_list['updated_on'])
    assert own_list['user_id'] == edited_list['user_id']


def test_del_list_for_valid_user(happy, happy_lists):
    own_list = happy_lists['active'][0]
    url = LIST_URL.format(listid=own_list['id'])
    auth = (happy['email'], happy['password'])
    resp = requests.delete(url, auth=auth)
    assert 204 == resp.status_code

    resp = requests.get(LISTS_URL, auth=auth)
    all_lists = resp.json()['usergen']
    assert own_list['id'] not in [list_['id'] for list_ in all_lists]


# User tries to edit a list which does not exist
def test_edit_del_absent_list(happy):
    url = LIST_URL.format(listid=1000)
    auth = (happy['email'], happy['password'])

    params = {"name": "some name"}
    resp = requests.patch(url, auth=auth, headers=JSON_HEADERS, data=json.dumps(params))
    assert 403 == resp.status_code

    resp = requests.delete(url, auth=auth)
    assert 403 == resp.status_code


# User tries to edit a list which they do not own
def test_edit_del_other_list(happy, frozen_lists):
    other_list = frozen_lists['active'][0]
    url = LIST_URL.format(listid=other_list['id'])
    auth = (happy['email'], happy['password'])

    params = {"name": "some name"}
    resp = requests.patch(url, auth=auth, headers=JSON_HEADERS, data=json.dumps(params))
    assert 403 == resp.status_code

    resp = requests.delete(url, auth=auth)
    assert 403 == resp.status_code


# User tries to edit the default list
def test_edit_del_default_list(happy, happy_lists):
    default_list = happy_lists['default_']
    url = LIST_URL.format(listid=default_list['id'])
    auth = (happy['email'], happy['password'])

    params = {"name": "some name"}
    resp = requests.patch(url, auth=auth, headers=JSON_HEADERS, data=json.dumps(params))
    assert 403 == resp.status_code

    resp = requests.delete(url, auth=auth)
    assert 403 == resp.status_code


# User tries to edit the name of a list to (Default) or ""
def test_edit_list_to_default(happy, happy_lists):
    own_list = happy_lists['active'][0]
    url = LIST_URL.format(listid=own_list['id'])
    auth = (happy['email'], happy['password'])
    params = {"name": "(Default)"}
    resp = requests.patch(url, auth=auth, headers=JSON_HEADERS, data=json.dumps(params))
    assert 400 == resp.status_code


# User tries to edit the state of the list to something invalid
def test_edit_list_invalid_state(happy, happy_lists):
    own_list = happy_lists['active'][0]
    url = LIST_URL.format(listid=own_list['id'])
    auth = (happy['email'], happy['password'])
    params = {"state": "bad-state"}
    resp = requests.patch(url, auth=auth, headers=JSON_HEADERS, data=json.dumps(params))
    assert 400 == resp.status_code


# User passes in a non-numeric list id
def test_edit_del_list_badid(happy):
    url = LIST_URL.format(listid='hahaha')
    auth = (happy['email'], happy['password'])

    params = {"name": "some name"}
    resp = requests.patch(url, auth=auth, headers=JSON_HEADERS, data=json.dumps(params))
    assert 400 == resp.status_code

    resp = requests.delete(url, auth=auth)
    assert 400 == resp.status_code
