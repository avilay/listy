import csv
import os.path as path
from datetime import datetime, timezone, timedelta


def _gen_date(tmpl):
    if not tmpl:
        return None

    toks = tmpl.split()

    start = datetime.min
    if toks[0] == '$today':
        start = datetime.now(timezone.utc)
    elif toks[0] == '$yesterday':
        start = datetime.now(timezone.utc) - timedelta(days=1)
    elif toks[0] == '$tomorrow':
        start = datetime.now(timezone.utc) + timedelta(days=1)
    else:
        raise RuntimeError('{} - Unsupported date template!'.format(toks[0]))

    if len(toks) > 1:
        numdays = 0
        try:
            numdays = int(toks[2])
        except ValueError:
            raise RuntimeError('{} - Days must be a number!'.format(toks[2]))

        retdt = None
        if toks[1] == '-':
            retdt = start - timedelta(days=numdays)
        elif toks[1] == '+':
            retdt = start + timedelta(days=numdays)

        return retdt
    else:
        return start
    
    
class DataLoader:
    _instance = None
    DATAROOT = path.dirname(__file__)

    def __init__(self):
        self.users = []
        usersfile = path.join(DataLoader.DATAROOT, 'users.csv')
        with open(usersfile) as f:
            reader = csv.DictReader(row for row in f if row and not row.startswith('#'))
            for row in reader:
                row['created_on'] = _gen_date(row['created_on'])
                self.users.append(row)

        self.passwords = {}
        pwdsfile = path.join(DataLoader.DATAROOT, 'passwords.csv')
        with open(pwdsfile) as f:
            reader = csv.DictReader(row for row in f if not row.startswith('#'))
            for row in reader:
                self.passwords[row['email']] = row['password']

        self.lists = []
        listsfile = path.join(DataLoader.DATAROOT, 'lists.csv')
        with open(listsfile) as f:
            reader = csv.DictReader(row for row in f if not row.startswith('#'))
            for row in reader:
                row['created_on'] = _gen_date(row['created_on'])
                row['updated_on'] = _gen_date(row['updated_on'])
                self.lists.append(row)

        self.tasks = []
        tasksfile = path.join(DataLoader.DATAROOT, 'tasks.csv')
        with open(tasksfile) as f:
            reader = csv.DictReader(row for row in f if not row.startswith('#'))
            for row in reader:
                row['created_on'] = _gen_date(row['created_on'])
                row['scheduled_start_on'] = _gen_date(row['scheduled_start_on'])
                row['last_start_on'] = _gen_date(row['last_start_on'])
                row['completed_on'] = _gen_date(row['completed_on'])
                if row['estimated_minutes']:
                    row['estimated_minutes'] = int(row['estimated_minutes'])
                else:
                    row['estimated_minutes'] = None
                if not row['description']:
                    row['description'] = None
                self.tasks.append(row)

    @classmethod
    def instance(cls):
        if not cls._instance:
            cls._instance = DataLoader()
        return cls._instance

