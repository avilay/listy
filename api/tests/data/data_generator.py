import psycopg2 as pg
import psycopg2.extras as pgextras

from api.tests.data.data_loader import DataLoader


def _truncate_table(db, table):
    cur = db.cursor()
    sql = 'TRUNCATE TABLE {} CASCADE'.format(table)
    cur.execute(sql)
    cur.close()
    db.commit()


def _insert(db, table, objs, attrs):
    cur = db.cursor()

    sql = 'INSERT INTO {} ('.format(table)
    for attr in attrs:
        sql += attr + ', '
    sql = sql.rstrip(', ')
    sql += ') VALUES ({})'
    args = ('%s, ' * len(attrs)).rstrip(', ')
    sql = sql.format(args)
    ids = []
    for obj in objs:
        params = [obj[attr] for attr in attrs]
        cur.execute(sql, params)
        ids.append(int(obj['id']))
    db.commit()

    next_id = max(ids) + 1
    seqname = '{}_id_seq'.format(table)
    cur.execute('ALTER SEQUENCE {} RESTART WITH %s'.format(seqname), [next_id])
    db.commit()

    cur.close()


def _select_all(db, table, attrs):
    cur = db.cursor(cursor_factory=pgextras.RealDictCursor)
    sql = 'SELECT '
    for attr in attrs:
        sql += attr + ', '
    sql = sql.rstrip(', ')
    sql += ' FROM {}'.format(table)
    cur.execute(sql)
    rows = cur.fetchall()
    cur.close()
    return rows


def gen_users(db):
    attrs = ['id', 'name', 'email', 'password_hash', 'created_on']
    dl = DataLoader.instance()
    objs = dl.users
    _truncate_table(db, 'users')
    _insert(db, 'users', objs, attrs)
    users = _select_all(db, 'users', attrs)
    for user in users:
        user['password'] = dl.passwords[user['email']]
    return users


def gen_lists(db):
    attrs = ['id', 'user_id', 'name', 'created_on', 'updated_on', 'state']
    dl = DataLoader.instance()
    objs = dl.lists
    _truncate_table(db, 'lists')
    _insert(db, 'lists', objs, attrs)
    return _select_all(db, 'lists', attrs)


def gen_tasks(db):
    attrs = ['id', 'list_id', 'title', 'state', 'created_on', 'minutes_spent', 'description', 'scheduled_start_on', 'last_start_on', 'estimated_minutes', 'completed_on']
    dl = DataLoader.instance()
    objs = dl.tasks
    _truncate_table(db, 'tasks')
    _insert(db, 'tasks', objs, attrs)
    return _select_all(db, 'tasks', attrs)


def main():
    db = pg.connect(host='localhost', database='listy', user='avilay.parekh')
    # db = pg.connect(host='avilay.info', database='listy', user='dbuser', password='pony123')
    gen_users(db)
    gen_lists(db)
    gen_tasks(db)

if __name__ == '__main__':
    print('GENERATING DATA')
    main()
