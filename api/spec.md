# Authentication
Except a few open APIs, most follow a Basic Authentication scheme with user email as the username and the password.

## Errors
A `401 - Unauthorized` is returned if -

* email is not present in the db
* password is incorrect


# Lists
Users can group their tasks (or todos) in various lists. A user can have multiple lists. Each list can have multiple tasks. When a user is created, they will have a list named (Default) created for them. Lists can be in one the following two states -

* active
* archived


## Get all lists
All users will have the (Default) list at a minimum.

    GET /lists
    Authorization: happy:orange

    200
    Content-Type: application/json
    {
        "default_": {
            "id": 1,
            "user_id": 1,
            "created_on": "2016-04-02T00:36:12+00:00",
            "updated_on": "2016-05-28T00:36:12+00:00",
            "name": "(Default)",
            "state": "active"
        },
        "usergen": [
            {
                "id": 2,
                "user_id": 1,
                "created_on": "2016-05-28T00:36:12+00:00",
                "updated_on": "2016-05-28T00:36:12+00:00",
                "name": "My First List",
                "state": "active"
            },
            {
                "id": 3,
                "user_id": 1,
                "created_on": "2016-05-28T00:36:12+00:00",
                "updated_on": "2016-05-28T00:36:12+00:00",
                "name": "My Second Active List",
                "state": "active"
            },
            {
                "id": 4,
                "user_id": 1,
                "created_on": "2016-05-28T00:36:12+00:00",
                "updated_on": "2016-05-28T00:36:12+00:00",
                "name": "My Archived List",
                "state": "archived"
            }
        ]
    }

## Get a specific list

    GET /lists/2
    Authorization: happy:orange

    200
    Content-Type: application/json
    {
        "id": 2,
        "user_id": 1,
        "created_on": "2016-05-28T00:36:12+00:00",
        "updated_on": "2016-05-28T00:36:12+00:00",
        "name": "My First List",
        "state": "active"
    }


## Create a new list

    POST /lists
    Authorization: happy:orange
    {
        "name": "This is a required parameter"
    }

    200
    Content-Type: application/json
    {
        "id": 5,
        "user_id": 1,
        "created_on": "2016-05-28T00:36:12+00:00",
        "updated_on": "2016-05-28T00:36:12+00:00",
        "name": "Brand New Things To Do",
        "state": "active"
    }

### Errors
A `400 - Bad Request` is returned if -

* "name" attribute is not present in the request JSON
* "name" is set to "(Default)" or a blank string ""


## Edit a list
It is possible to change the name of the list or its state. But it is not possible to edit the attributes of the (Default) list.

    PATCH /lists/2
    Authorization: happy:orange
    {
        "name": "Changed Name"
        "state": "active"
    }

    200
    Content-Type: application/json
    {
        "id": 2,
        "user_id": 1,
        "created_on": "2016-05-28T00:36:12+00:00",
        "updated_on": "2016-05-28T00:36:12+00:00",
        "name": "Changed Name",
        "state": "active"
    }

### Errors
A `400 - Bad Reqeust` is raised if -

* list id is not a number
* "name" is set to "(Default)" or a blank string ""
* "state" is set to anything other than "active" or "archived"

A `403 - Forbidden` is raised if -

* list id belongs to the user's (Default) list
* list id does not belong to the user
* list id does not exist in the db


## Delete a list
Instead of archiving, the user may wish to completely delete a list. This is a permanent action. Users can delete any user-generated list, but they cannot delete the (Default) list.

    DELETE /lists/2
    Authorization: happy:orange
    
    204
    
### Errors
A `400 - Bad Request` is raised if -

* list id is not a number

A `403 - Forbidden` is raised if -

* list id belongs to the user's (Default) list
* list id does not belong to the user
* list id does not exist in the db


# Tasks
Tasks are grouped together in a list. A single task can only belong to a single list. A task can be in one of the following states -

* not-started
* scheduled
* in-progress
* paused
* done

All tasks start in the _not-started_ state.


## Get tasks in a list
Gets all the tasks that were ever a part of this list, even tasks that were completed a long time ago.

    GET /lists/2/tasks
    Authorization: happy:orange

    200
    [
        {
            "id": 1,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Scheduled but not started",
            "state": "scheduled",
            "minutes_spent": 0,
            "estimated_minutes": null
        },
        {
            "id": 2,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Something has been done",
            "state": "in-progress",
            "minutes_spent": 20,
            "estimated_minutes": 90
        },
        {
            "id": 3,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Just listin",
            "state": "not-started",
            "minutes_spent": 0,
            "estimated_minutes": 20
        },
        {
            "id": 4,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Not interested in doing anymore",
            "state": "paused",
            "minutes_spent": 100,
            "estimated_minutes": 15
        },
        {
            "id": 5,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Done a couple of days ago",
            "state": "done",
            "minutes_spent": 100,
            "estimated_minutes": 120
        },
        {
            "id": 6,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Done a long time ago",
            "state": "done",
            "minutes_spent": 100,
            "estimated_minutes": null
        }
    ]


If a user does not have any tasks in a list return an empty array.

    GET /lists/3/tasks
    Authorization: happy:orange

    200
    Content-Type: application/json
    []


Alternatively, if a filter query string parameter is provided, then it will get only tasks that were completed in the last 3 days along with all other in-complete tasks.

    GET /lists/2/tasks?filter=recent
    Authorization: happy:orange

    200
    [
        {
            "id": 1,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Scheduled but not started",
            "state": "scheduled",
            "minutes_spent": 0,
            "estimated_minutes": null
        },
        {
            "id": 2,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Something has been done",
            "state": "in-progress",
            "minutes_spent": 20,
            "estimated_minutes": 90
        },
        {
            "id": 3,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Just listin",
            "state": "not-started",
            "minutes_spent": 0,
            "estimated_minutes": 20
        },
        {
            "id": 4,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Not interested in doing anymore",
            "state": "paused",
            "minutes_spent": 100,
            "estimated_minutes": 15
        },
        {
            "id": 5,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Done a couple of days ago",
            "state": "done",
            "minutes_spent": 100,
            "estimated_minutes": 120
        }
    ]

### Errors
A `400 - Bad Request` is raised if -

* list id is not a number
* filter is set to anything other than "recent"

A `403 - Forbidden` is raised if -

* list id does not belong to the user
* list id does not exist in the db



## Get scheduled tasks
Gets all scheduled tasks, in-progress tasks, paused tasks, and tasks that were done today, from all the lists.

    GET /scheduled-tasks
    Authorization: happy:orange

    200
    Content-Type: application/json
    [
        {
            "id": 1,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Scheduled but not started",
            "state": "scheduled",
            "minutes_spent": 0,
            "estimated_minutes": null
        },
        {
            "id": 2,
            "list_id": 2,
            "list_name": "My First List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Something has been done",
            "state": "in-progress",
            "minutes_spent": 20,
            "estimated_minutes": 90
        },
        {
            "id": 3,
            "list_id": 3,
            "list_name": "My Second List",
            "created_on": "2016-05-28T00:36:12+00:00",
            "title": "Just completed",
            "state": "done",
            "minutes_spent": 120,
            "estmiated_minutes": 0
        }
    ]


If a user does not have any scheduled tasks, return an empty array

    GET /scheduled-tasks
    Authorization: frozen:horizon

    200
    Content-Type: application/json
    []
        

## Get task details

    GET /lists/2/tasks/3
    Authorization: happy:orange

    200
    {
        "id": 1,
        "list_id": 2,
        "list_name": "My Second List",
        "created_on": "2016-05-28T00:36:12+00:00",
        "title": "Just completed",
        "state": "done",
        "minutes_spent": 0,
        "estimated_minutes": 60,
        "description": "Assertively redefine top-line technologies without an expanded array of testing procedures.",
        "scheduled_start_on": "2016-05-28T00:36:12+00:00",
        "last_start_on": "2016-05-28T00:36:12+00:00",
        "completed_on": "2016-05-28T00:36:12+00:00"
    }


## Create a new task
Can only create one task at a time. 

    POST /lists/2/tasks
    Authorization: happy:orange
    Content-Type: application/json
    {
        "list_id": 2,
        "title": "Only required params here"
    }

    200
    {
        "id": 31,
        "list_id": 2,
        "list_name": "My Second List",
        "created_on": "2016-05-28T00:36:12+00:00",
        "title": "Only required params here",
        "state": "not-started",
        "minutes_spent": 0
    }

    
## Edit a task
It is possible to change the title, description, scheduled\_start\_on, and estimated\_minutes. Only the following state changes are valid -

* not-started --> scheduled
* scheduled --> in-progress
* scheduled --> done
* in-progress --> done
* in-progress --> paused
* paused --> in-progress

Any other state transition will result in a 406 (Not Acceptable) status code.

    PATCH /lists/2/tasks/1
    Authorization: happy:orange
    {
        "obj": {
            "state": "scheduled",
            "estimated_minutes": 20,
            "scheduled_start_on": "2016-05-28T00:36:12+00:00"
        }
    }

    200
    Content-Type: application/json
    {
        "id": 31,
        "list_id": 2,
        "list_name": "My Second List",
        "created_on": "2016-05-28T00:36:12+00:00",
        "title": "Only required params here",
        "state": "scheduled",
        "minutes_spent": 0,
        "estimated_minutes": 20,
        "scheduled_start_on": "2016-05-28T00:36:12+00:00"
    }


## Delete a task
It is possible to delete any task regardless of the state it may be in.

    DELETE /lists/2/tasks/1
    Authorization: happy:orange
    
    204