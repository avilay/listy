import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class Debug extends Component {

  constructor(props) {
    super(props);
    this.state = {items: ['hello', 'world', 'click', 'me']};

    this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd() {
    let newItems =
      this.state.items.concat([prompt('Enter some text')]);
    this.setState({items: newItems});
  }

  render() {
    let items = this.state.items.map(function(item, i) {
      return (
        <div key={item} onClick={this.handleRemove.bind(this, i)}>
          {item}
        </div>
      );
    }.bind(this));
    return (
      <div className="container">
        <button onClick={this.handleAdd}>Add Item</button>
        <ReactCSSTransitionGroup 
          transitionName="example" 
          transitionEnterTimeout={500} 
          transitionLeaveTimeout={300}>
          {items}
        </ReactCSSTransitionGroup>
      </div>
    );
  }

}

export default Debug;