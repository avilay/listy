import React, { Component, PropTypes } from 'react';

class Button extends Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.preventDefault();
    this.props.onClick();
  }

  render() {
    let className = 'btn ' + this.props.style.type + ' ' + this.props.style.size;
    let type = this.props.type ? this.props.type : 'button';
    let disabled = (this.props.isSpinning || this.props.isDisabled) ? 'disabled' : ''; 

    let icon = null;
    let iconClass = '';
    if (this.props.isSpinning) {
      iconClass = 'fa fa-spinner fa-spin';
    } else if (this.props.icon) {
      iconClass = 'fa ' + this.props.icon.type;
    }
    if (this.props.icon && this.props.icon.size) {
      iconClass += ' ' + this.props.icon.size;
    }
    if (iconClass) {
      icon = <i className={iconClass} />;
    }

    return (
      <button
        className={className}
        style={this.props.cssStyle}
        type={type}
        disabled={disabled}
        onClick={this.props.onClick}>
          {this.props.value}
          {icon}
      </button>
    );

  }

}

Button.propTypes = {
  style: PropTypes.shape({
    size: PropTypes.string,
    type: PropTypes.string
  }).isRequired,

  cssStyle: PropTypes.object,

  type: PropTypes.string,
  value: PropTypes.string,
  icon: PropTypes.shape({
    size: PropTypes.string,
    type: PropTypes.string
  }),
  isSpinning: PropTypes.bool,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool
};

export default Button;