import React, { Component, PropTypes } from 'react';

class Flash extends Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.preventDefault();
    if (this.props.onClick) {
      this.props.onClick(this.props.id);      
    }
  }

  render() {
    console.debug('Flash::render');
    let style = {padding: "10px"};
      
    let className = null;
    if (this.props.type === 'info') {
      className = 'bg-info';
    } else if (this.props.type === 'warn') {
      className = 'bg-warning';
    } else if (this.props.type === 'error') {
      className = 'bg-danger';
    }

    return(
      <div className={className} style={style}>
        <button type="button" className="close" aria-label="Close" onClick={this.onClick}>
          <span aria-hidden="true">&times;</span>
        </button>
          {this.props.msg}
      </div>
    );
  }

}

Flash.propTypes = {
  type: PropTypes.string.isRequired,
  msg: PropTypes.string.isRequired,
  onClick: PropTypes.func
};

export default Flash;