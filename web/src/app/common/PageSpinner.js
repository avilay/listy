import React, { Component, PropTypes } from 'react';

class PageSpinner extends Component {

  render() {
    return(
      <div className="container">
        <div className="page-header">
          <h1>{this.props.pageHeader}</h1>
        </div>
        <div>
          <i className="fa fa-2x fa-spinner fa-spin"></i>
        </div>
      </div>
    );
  }

}

PageSpinner.propTypes = {
  pageHeader: PropTypes.string.isRequired
};

export default PageSpinner;