import React, { Component, PropTypes } from 'react';
import Link from '../router/Link';

class Navbar extends Component {
  render() {
    let button = (
      <a href="/login" type="button" className="btn btn-nav">Login</a>
    );
    if (this.props.username) {
      button = (
        <button type="button" className="btn btn-nav dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {this.props.username} <span className="caret"></span>
        </button>
      );
    }
  
    return (
      <div>
        <nav className="navbar navbar-inverse navbar-fixed-top">
          <div className="container-fluid">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <Link href="/welcome" className="navbar-brand">Listy</Link>
            </div>
            <div id="navbar" className="navbar-collapse collapse">
              <ul className="nav navbar-nav navbar-right">
                <li><Link href="/lists">Lists</Link></li>
                <li><Link href="/scheduled">Scheduled</Link></li>
                <li>
                  <div className="btn-group">
                    {button}
                    <ul className="dropdown-menu">
                      <li><Link href="/profile">Profile</Link></li>
                      <li><a href="/logout">Logout</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div> 
    );
  }
}

Navbar.propTypes = {
  username: PropTypes.string
}

export default Navbar;