import React, { Component, PropTypes } from 'react';
import Button from '../common/Button'; 

class NewListButton extends Component {

  render() {
    let style = {marginBottom: '20px'};
    return (
      <div className="row">
        <div className="col-md-2" style={style}>
          <Button style={{type: 'btn-default', size: 'btn-sm'}}
            onClick={this.props.onClick}
            value={this.props.value} />
        </div>
      </div>
    );
  }

}

NewListButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  value: PropTypes.string
};

export default NewListButton;