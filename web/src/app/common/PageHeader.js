import React, { Component, PropTypes } from 'react';

class PageHeader extends Component {

  render() {
    let styleCls = "page-header " + this.props.className;
    return (
      <div className={styleCls}>
        <h1>{this.props.value}</h1>
      </div>
    );
  }

}

PageHeader.propTypes = {
  value: PropTypes.string.isRequired,
  className: PropTypes.string
};

export default PageHeader;