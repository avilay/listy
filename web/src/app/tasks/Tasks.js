import React, { Component, PropTypes } from 'react';
import PageHeader from '../common/PageHeader';
import createTaskStore from '../store/TaskStore';
import createListStore from '../store/ListStore';
import PageSpinner from '../common/PageSpinner';
import Task from './Task';
import NewItemButton from '../common/NewItemButton';
import InProgressTask from './InProgressTask';
import PausedTask from './PausedTask';
import DoneTask from './DoneTask';
import ArchivedTask from './ArchivedTask';
import TaskForm from './form/TaskForm';
import { handleFetch, genFlashMsg } from '../utils';
import Flash from '../common/Flash';


class Tasks extends Component {
  
  constructor(props) {
    super(props);
    this.taskStore = createTaskStore();
    this.listStore = createListStore();
    this.list = null;

    this.state = {
      flashMsgs: [],
      view: 'tasks',
      isLoading: true,
      tasks: new Map(),
      listName: 'Loading'
    };

    this.refreshAll = this.refreshAll.bind(this);
    
    this.onNewTask = this.onNewTask.bind(this);
    this.onNewTaskSubmit = this.onNewTaskSubmit.bind(this);
    this.onNewTaskCancel = this.onNewTaskCancel.bind(this);

    this.changeState = this.changeState.bind(this);

    this.onFlashClick = this.onFlashClick.bind(this);
  }

  onFlashClick(id) {
    let flashMsgs = this.state.flashMsgs;
    flashMsgs.splice(id, 1);
    this.setState({flashMsgs});
  }

  componentDidMount() {
    this.listStore.getList(this.props.listId)
    .then(resp => handleFetch(resp))
    .then(list => {
      this.list = list;
      this.setState({listName: list.name});
      return this.taskStore.getTasks(this.props.listId);
    })
    .then(resp => handleFetch(resp))
    .then(tasks => this.refreshAll(tasks))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        isLoading: false,
        flashMsgs: flashMsgs
      });
    });
  }

  changeState(taskId, newState) {
    this.taskStore.changeTaskState(this.props.listId, taskId, newState)
    .then(resp => handleFetch(resp))
    .then(task => this.refreshOne(task))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        isLoading: false,
        flashMsgs: flashMsgs,
        view: 'tasks'
      });
    });
  }

  onNewTask(e) {
    e.preventDefault();
    this.setState({view: 'newTask'});
  }

  onNewTaskSubmit(title, estMins, startOn, desc) {
    this.taskStore.createTask(this.props.listId, title, estMins, startOn, desc)
    .then(resp => handleFetch(resp))
    .then(task => this.refreshOne(task))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        isLoading: false,
        flashMsgs: flashMsgs,
        view: 'tasks'
      });
    });
  }

  onNewTaskCancel() {
    this.setState({view: 'tasks'});
  }

  refreshAll(tasks) {
    let taskMap = new Map();
    for (let task of tasks) {
      taskMap.set(task.id, task);
    }

    this.setState({
      view: 'tasks',
      isLoading: false,
      tasks: taskMap
    });
  }

  refreshOne(task) {
    let tasks = this.state.tasks;
    tasks.set(task.id, task);
    this.setState({
      tasks: tasks,
      view: 'tasks'
    });
  }

  renderFlashMsgs() {
    let flashes = [];
    for (let i = 0; i < this.state.flashMsgs.length; i++) {
      let flashMsg = this.state.flashMsgs[i];
      flashes.push(<Flash key={i+1} id={i} onClick={this.onFlashClick} type="error" msg={flashMsg} />);
    }
    return flashes;
  }

  renderActiveTasksView() {
    let tasksView = [];
    let listId = this.props.listId;
    for (let key of this.state.tasks.keys()) {
      let task = this.state.tasks.get(key);
      if (task.state === 'scheduled' || task.state === 'not-started') {
        tasksView.push(<Task key={task.id} listId={listId} task={task} changeState={this.changeState} />);
      } else if (task.state === 'in-progress') {
        tasksView.push(<InProgressTask key={task.id} listId={listId} task={task} changeState={this.changeState} />);
      } else if (task.state === 'paused') {
        tasksView.push(<PausedTask key={task.id} listId={listId} task={task} changeState={this.changeState} />);
      } else if (task.state === 'done') {
        tasksView.push(<DoneTask key={task.id} listId={listId} task={task} changeState={this.changeState} />);
      }
    }

    return (
      <div className="container">
        {this.renderFlashMsgs()}
        <PageHeader value={this.list.name} />
        <NewItemButton onClick={this.onNewTask} value='New Task' />
        {tasksView}
      </div>
    );   
  }

  renderArchivedTasksView() {
    let tasksView = [];
    for (let key of this.state.tasks.keys()) {
      let task = this.state.tasks.get(key);
      tasksView.push(<ArchivedTask key={task.id} listId={this.props.listId} task={task} />)
    }

    return(
      <div className="container">
        {this.renderFlashMsgs()}
        <PageHeader value={this.list.name} />
        {tasksView}
      </div>
    );
  }

  renderTasksView() {
    if (this.list.state === 'active') {
      return this.renderActiveTasksView();
    } else if (this.list.state === 'archived') {
      return this.renderArchivedTasksView();
    }
  }

  render() {
    if (this.state.isLoading) {
      return (<PageSpinner pageHeader={this.state.listName} />);
    }

    if (this.state.view === 'tasks') {
      return this.renderTasksView();
    } else if (this.state.view === 'newTask') {
      return (<TaskForm listName={this.state.listName} onCancel={this.onNewTaskCancel} onSubmit={this.onNewTaskSubmit} />);
    }
  }

}

Tasks.propTypes = {
  listId: PropTypes.number.isRequired
};

export default Tasks;