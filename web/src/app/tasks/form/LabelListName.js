import React, { Component, PropTypes } from 'react';

class LabelListName extends Component {

  render() {
    return (
      <div className="form-group">
        <label>List</label>
        <div>{this.props.listName}</div>
      </div>
    );
  }

}

LabelListName.proptTypes = {
  listName: PropTypes.string.isRequired
};

export default LabelListName;