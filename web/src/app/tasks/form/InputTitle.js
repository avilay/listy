import React, { Component, PropTypes } from 'react';

class InputTitle extends Component {

  render() {
    return (
      <div className="page-header">
        <div className="row">
          <div className="col-md-12">
            <input 
              type="text" 
              className="form-control input-lg" 
              placeholder="Task title here" 
              onChange={this.props.onTitleChange}
              value={this.props.title || ''} />
          </div>
        </div>
      </div>
    );
  }
  
}

InputTitle.propTypes = {
  onTitleChange: PropTypes.func.isRequired,
  title: PropTypes.string
};

export default InputTitle;