import React, { Component, PropTypes } from 'react';

class InputDescription extends Component {

  render() {
    return(
      <div className="form-group">
        <label>Description</label>
        <textarea 
          rows="3" 
          className="form-control" 
          onChange={this.props.onDescChange}
          value={this.props.description || ''}></textarea>
      </div>
    );
  }

}

InputDescription.propTypes = {
  onDescChange: PropTypes.func.isRequired,
  description: PropTypes.string
};

export default InputDescription;