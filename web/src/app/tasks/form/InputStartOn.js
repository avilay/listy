import React, { Component, PropTypes } from 'react';
import Datetime from 'react-datetime';

class InputStartOn extends Component {

  render() {
    return (
      <div className="form-group">
        <label>Starting On</label>
        <div className="row">
          <div className="col-md-5">
            <Datetime dateFormat='DD MMM' timeFormat={false} onChange={this.props.onStartChange} value={this.props.start} />
          </div>
        </div>
      </div>
    );
  }

}

InputStartOn.propTypes = {
  onStartChange: PropTypes.func.isRequired,
  start: PropTypes.object
};

export default InputStartOn;