import React, { Component, PropTypes } from 'react';
import Button from '../../common/Button';
import Flash from '../../common/Flash';
import { minsToHrs } from '../../utils';
import moment from 'moment';
import InputTitle from './InputTitle';
import InputEstimate from './InputEstimate';
import InputDescription from './InputDescription';
import LabelListName from './LabelListName';
import InputStartOn from './InputStartOn';
import LabelStartOn from './LabelStartOn';

class TaskForm extends Component {

  constructor(props) {
    super(props);
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.onTitleChange = this.onTitleChange.bind(this);
    this.onHrChange = this.onHrChange.bind(this);
    this.onMinChange = this.onMinChange.bind(this);
    this.onStartChange = this.onStartChange.bind(this);
    this.onDescChange = this.onDescChange.bind(this);

    this.onFlashClick = this.onFlashClick.bind(this);

    let task = {};
    if (this.props.task) {
      task.title = this.props.task.title;

      if (this.props.task.description) {
        task.description = this.props.task.description;
      }

      if (this.props.task.scheduled_start_on) {
        task.scheduled_start_on = this.props.task.scheduled_start_on;
        task.start = moment(this.props.task.scheduled_start_on);
      }
      
      if (this.props.task.estimated_minutes) {
        task.estimated_minutes = this.props.task.estimated_minutes;
        let est = minsToHrs(this.props.task.estimated_minutes);
        task.hrs = est.hrs;
        task.mins = est.mins;
      }

      task.state = this.props.task.state;
    }

    this.state = {
      submitting: false,
      flashMsgs: [],
      task: task
    };
  }

  onSubmit(e) {
    e.preventDefault();
    let flashMsgs = [];
    let newTask = {};
    let task = this.state.task;

    if (this.props.task) {
      if (!task.title) {
        flashMsgs.push('You must provide a task title!');
        task.title = this.props.task.title;
      } else {
        newTask.title = task.title;
      }

      if (task.description !== this.props.task.description) {
        newTask.description = task.description;
      }

      if (!task.scheduled_start_on && this.props.task.scheduled_start_on) {
        flashMsgs.push('You cannot unschedule an already scheduled task!');
        task.scheduled_start_on = this.props.task.scheduled_start_on;
        task.start = moment(this.props.task.scheduled_start_on);
      } else if (task.scheduled_start_on && task.scheduled_start_on !== this.props.task.scheduled_start_on) {
        newTask.scheduled_start_on = task.scheduled_start_on;
      }

      if (task.estimated_minutes !== this.props.task.estimated_minutes) {
        newTask.estimated_minutes = task.estimated_minutes;
      }
    } else {
      if (!task.title) {
        flashMsgs.push('You must provide a task title!');
      }
      newTask = task;
    }

    if (flashMsgs.length > 0) {
      this.setState({task, flashMsgs});
    } else {
      this.props.onSubmit(newTask);
      this.setState({submitting: true});
    }

  }

  onFlashClick(id) {
    let flashMsgs = this.state.flashMsgs;
    flashMsgs.splice(id, 1);
    this.setState({flashMsgs});
  }

  onTitleChange(e) {
    let title = e.target.value;
    let task = this.state.task;
    task.title = title;
    this.setState({task})
  }

  onHrChange(e) {
    let task = this.state.task;
    task.hrs = e.target.value;
    let hrs = parseInt(task.hrs, 10);
    let mins = parseInt(task.mins, 10);
    task.estimated_minutes = (hrs || 0) * 60 + (mins || 0);
    this.setState({task});
  }

  onMinChange(e) {
    let task = this.state.task;
    task.mins = e.target.value;
    let hrs = parseInt(task.hrs, 10);
    let mins = parseInt(task.mins, 10);
    task.estimated_minutes = (hrs || 0) * 60 + (mins || 0);
    this.setState({task});
  }

  onStartChange(e) {
    let task = this.state.task;
    task.start = e;
    if (task.start) {
      task.scheduled_start_on = task.start.toISOString();
    } else {
      task.scheduled_start_on = null;
    }
    this.setState({task});
  }

  onDescChange(e) {
    let desc = e.target.value;
    let task = this.state.task;
    task.description = desc;
    this.setState({task});
  }

  onCancel(e) {
    e.preventDefault();
    this.props.onCancel();
  }

  render() {
    let flashes = [];
    for (let i = 0; i < this.state.flashMsgs.length; i++) {
      let flashMsg = this.state.flashMsgs[i];
      flashes.push(<Flash key={i+1} id={i} onClick={this.onFlashClick} type="error" msg={flashMsg} />);
    }

    let startOn = null;
    if (!this.state.task.state || this.state.task.state === 'not-started' || this.state.task.state === 'scheduled') {
      startOn = <InputStartOn onStartChange={this.onStartChange} start={this.state.task.start} />
    } else {
      startOn = <LabelStartOn start={this.state.task.start} />
    }

    return(
      <div className="container">
        {flashes}
        <form onSubmit={this.onSubmit}>
          <InputTitle title={this.state.task.title} onTitleChange={this.onTitleChange} />
          <div className="row">
            <div className="col-md-6">
              <LabelListName listName={this.props.listName} />
              <InputEstimate 
                onHrChange={this.onHrChange} hrs={this.state.task.hrs} 
                onMinChange={this.onMinChange} mins={this.state.task.mins} />
              {startOn}
            </div>
            <div className="col-md-6">
              <InputDescription onDescChange={this.onDescChange} description={this.state.task.description} />
            </div>
          </div>
        <div className="row">
          <div className="col-md-6">            
            <Button 
              type="submit" 
              style={{type: 'btn-primary'}} 
              cssStyle={{marginRight: "10px"}} 
              value="Save" 
              isSpinning={this.state.submitting} />
            <Button 
              style={{type: 'btn-default'}} 
              value="Cancel" 
              onClick={this.onCancel} 
              isDisabled={this.state.submitting} />
          </div>
        </div>
        </form>
      </div>
    );
  }

}

TaskForm.propTypes = {
  listName: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  task: PropTypes.shape({
    id: PropTypes.number.isRequired,
    list_id: PropTypes.number.isRequired,
    list_name: PropTypes.string.isRequired,
    created_on: PropTypes.string,
    title: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired,
    minutes_spent: PropTypes.number,
    estimated_minutes: PropTypes.number,
    scheduled_start_on: PropTypes.string
  })
};

export default TaskForm;