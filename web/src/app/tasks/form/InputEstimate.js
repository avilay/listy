import React, { Component, PropTypes } from 'react';

class InputEstimate extends Component {

  render() {
    return(
      <div className="form-group">
        <label>Estimated Time</label>
        <div className="row">
          <div className="col-md-3">
            <div className="input-group">
              <input 
                type="text" 
                className="form-control" 
                aria-describedby="hourunit" 
                onChange={this.props.onHrChange} 
                value={this.props.hrs || ''} />
              <span className="input-group-addon" id="hourunit">hrs</span>
            </div>
          </div>
          <div className="col-md-3">
            <div className="input-group">
              <input 
                type="text" 
                className="form-control" 
                aria-describedby="minuteunit" 
                onChange={this.props.onMinChange} 
                value={this.props.mins || ''} />
              <span className="input-group-addon" id="minuteunit">mins</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

InputEstimate.propTypes = {
  onHrChange: PropTypes.func.isRequired,
  hrs: PropTypes.node,
  onMinChange: PropTypes.func.isRequired,
  mins: PropTypes.node  
};

export default InputEstimate;