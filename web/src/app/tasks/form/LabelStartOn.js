import React, { Component, PropTypes } from 'react';
import moment from 'moment';

class LabelStartOn extends Component {

  render() {
    return (
      <div className="form-group">
        <label>Starting On</label>
        <div className="row">
          <div className="col-md-5">
            <div>{moment(this.props.start).format('DD MMM')}</div>
          </div>
        </div>
      </div>
    );
  }

}

LabelStartOn.propTypes = {
  start: PropTypes.object
};

export default LabelStartOn;