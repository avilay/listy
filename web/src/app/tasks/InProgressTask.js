import React from 'react';
import Task from './Task';
import Button from '../common/Button';
import Checkbox from './Checkbox';
import Link from '../router/Link';

class InProgressTask extends Task {

  render() {
    let link = '/lists/' + this.props.listId + '/tasks/' + this.props.task.id;

    return(
      <div>
        <div className="row">
          <div className="col-xs-4 col-sm-8">
            <Checkbox isSpinning={this.state.doing} onClick={this.onDone} />
            <Link href={link} className="title"><strong>{this.props.task.title}</strong></Link>
          </div>
          <div className="col-xs-3 col-sm-2">
            <small>
                {this.estTime}
            </small>
          </div>
          <div className="col-xs-3 col-sm-1">
            <strong>
                <small>{this.state.timeSpent.hrs}h</small>
                <blink>&nbsp;:&nbsp;</blink>
                <small>{this.state.timeSpent.mins}m</small>
            </strong>
          </div>
          <div className="col-xs-2 col-sm-1">
            <Button style={{type: 'btn-default', size: 'btn-xs'}}
              icon={{type: 'fa-pause'}}
              isSpinning={this.state.pausing}
              onClick={this.onPause} />
          </div>
        </div>
        <hr />
      </div>
    );
  }

}

export default InProgressTask;