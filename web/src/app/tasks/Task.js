import React, { Component, PropTypes } from 'react';
import Button from '../common/Button';
import Checkbox from './Checkbox';
import * as utils from '../utils';
import Link from '../router/Link';

class Task extends Component {

  constructor(props) {
    super(props);

    let task = this.props.task;
    this.estTime = '';
    if (task.estimated_minutes) {
      let est = utils.minsToHrs(task.estimated_minutes);
      this.estTime = est.hrs + 'h : ' + est.mins + 'm';
    }

    this.schedOn = '';
    this.isOverdue = false;
    if (task.scheduled_start_on) {
      let schedOn = new Date(task.scheduled_start_on);
      let today = new Date();
      this.isOverdue = schedOn.getTime() < today.getTime();
      this.schedOn = utils.dateToString(schedOn);
    }

    let minsSpent = utils.calcMinsSpent(task);
    let timeSpent = utils.minsToHrs(minsSpent);
    this.state = {
      timeSpent: timeSpent,
      playing: false,
      pausing: false,
      doing: false
    };

    this.onPlay = this.onPlay.bind(this);
    this.onPause = this.onPause.bind(this);
    this.onDone = this.onDone.bind(this);
  }

  onPlay() {
    this.setState({playing: true});
    this.props.changeState(this.props.task.id, 'in-progress');
  }

  onPause(e) {
    e.preventDefault();
    this.setState({pausing: true});
    this.props.changeState(this.props.task.id, 'paused');
  }

  onDone(e) {
    e.preventDefault();
    this.setState({doing: true});
    this.props.changeState(this.props.task.id, 'done');
  }

  render() {
    let rowClass = '';
    if (this.isOverdue) {
      rowClass = 'text-danger';
    }

    let link = '/lists/' + this.props.listId + '/tasks/' + this.props.task.id;

    return(
      <div className={rowClass}>
        <div className="row">
          <div className="col-xs-4 col-sm-8">
            <Checkbox isSpinning={this.state.doing} onClick={this.onDone} />
            <Link href={link} className="title">{this.props.task.title}</Link>
          </div>
          <div className="col-xs-3 col-sm-2">
            <small>
              {this.estTime}
            </small>
          </div>
          <div className="col-xs-3 col-sm-1">
            <small>
              {this.schedOn}
            </small>
          </div>
          <div className="col-xs-2 col-sm-1">
            <Button style={{type: 'btn-default', size: 'btn-xs'}}
              icon={{type: 'fa-play'}}
              isSpinning={this.state.playing}
              onClick={this.onPlay} />
          </div>
        </div>
        <hr />
      </div>
    );
  }

}

Task.PropTypes = {
  listId: PropTypes.number.isRequired,
  task: PropTypes.shape({
    id: PropTypes.number.isRequired,
    list_id: PropTypes.number.isRequired,
    list_name: PropTypes.string.isRequired,
    created_on: PropTypes.string,
    title: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired,
    minutes_spent: PropTypes.number,
    estimated_minutes: PropTypes.number,
    scheduled_start_on: PropTypes.string
  }).isRequired,
  onPlay: PropTypes.func.isRequired,
  onPause: PropTypes.func.isRequired,
  onDone: PropTypes.func.isRequired
};


export default Task;