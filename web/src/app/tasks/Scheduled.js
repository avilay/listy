import React, { Component } from 'react';
import createTaskStore from '../store/TaskStore';
import PageHeader from '../common/PageHeader';
import PageSpinner from '../common/PageSpinner';
import Task from './Task';
import InProgressTask from './InProgressTask';
import PausedTask from './PausedTask';
import DoneTask from './DoneTask';
import { handleFetch, genFlashMsg } from '../utils';
import Flash from '../common/Flash';

class Scheduled extends Component {

  constructor(props) {
    super(props);
    this.taskStore = createTaskStore();

    this.state = {
      flashMsgs: [],
      loading: true,
      tasks: new Map()
    }

    this.changeState = this.changeState.bind(this);
    this.refreshAll = this.refreshAll.bind(this);

    this.onFlashClick = this.onFlashClick.bind(this);
  }

  onFlashClick(id) {
    let flashMsgs = this.state.flashMsgs;
    flashMsgs.splice(id, 1);
    this.setState({flashMsgs});
  }

  changeState(taskId, newState) {
    let task = this.state.tasks.get(taskId);
    this.taskStore.changeTaskState(task.list_id, task.id, newState)
    .then(resp => handleFetch(resp))
    .then(task => this.refreshOne(task))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        loading: false,
        flashMsgs: flashMsgs
      });
    });
  }

  refreshOne(task) {
    let tasks = this.state.tasks;
    tasks.set(task.id, task);
    this.setState({tasks});
  }

  refreshAll(tasks) {
    let taskMap = new Map();
    for (let task of tasks) {
      taskMap.set(task.id, task);
    }

    this.setState({
      loading: false,
      tasks: taskMap
    })
  }

  componentDidMount() {
    this.taskStore.getScheduledTasks()
    .then(resp => handleFetch(resp))
    .then(tasks => this.refreshAll(tasks))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        loading: false,
        flashMsgs: flashMsgs
      });
    });
  }

  renderFlashMsgs() {
    let flashes = [];
    for (let i = 0; i < this.state.flashMsgs.length; i++) {
      let flashMsg = this.state.flashMsgs[i];
      flashes.push(<Flash key={i+1} id={i} onClick={this.onFlashClick} type="error" msg={flashMsg} />);
    }
    return flashes;
  }

  render() {
    if (this.state.loading) {
      return (<PageSpinner pageHeader='Scheduled' />);
    }

    let tasksView = [];
    for (let key of this.state.tasks.keys()) {
      let task = this.state.tasks.get(key);
      if (task.state === 'scheduled' || task.state === 'not-started') {
        tasksView.push(<Task key={task.id} listId={task.list_id} task={task} changeState={this.changeState} />);
      } else if (task.state === 'in-progress') {
        tasksView.push(<InProgressTask key={task.id} listId={task.list_id} task={task} changeState={this.changeState} />);
      } else if (task.state === 'paused') {
        tasksView.push(<PausedTask key={task.id} listId={task.list_id} task={task} changeState={this.changeState} />);
      } else if (task.state === 'done') {
        tasksView.push(<DoneTask key={task.id} listId={task.list_id} task={task} changeState={this.changeState} />);
      }
    }

    return (
      <div className="container">
        {this.renderFlashMsgs()}
        <PageHeader value='Scheduled' />
        {tasksView}
      </div>
    );
  }
  
}

export default Scheduled;
