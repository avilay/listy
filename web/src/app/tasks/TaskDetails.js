import React, { Component, PropTypes } from 'react';
import Button from '../common/Button';
import createTaskStore from '../store/TaskStore';
import createListStore from '../store/ListStore';
import PageSpinner from '../common/PageSpinner';
import PageHeader from '../common/PageHeader';
import { minsToHrs, dateToString, handleFetch, genFlashMsg } from '../utils';
import TaskForm from './form/TaskForm';
import Flash from '../common/Flash';

class TaskDetails extends Component {

  constructor(props) {
    super(props);

    this.listId = this.props.listId;
    this.taskId = this.props.taskId;

    this.list = null;
    this.task = null;
    this.taskStore = createTaskStore();
    this.listStore = createListStore();
    this.state = {
      flashMsgs: [],
      task: null, 
      loading: true,
      view: 'task'
    };
    
    this.onEdit = this.onEdit.bind(this);
    this.onEditCancel = this.onEditCancel.bind(this);
    this.onEditSubmit = this.onEditSubmit.bind(this);
    this.onDelete = this.onDelete.bind(this);

    this.onFlashClick = this.onFlashClick.bind(this);

    this.refresh = this.refresh.bind(this);
  }

  onFlashClick(id) {
    let flashMsgs = this.state.flashMsgs;
    flashMsgs.splice(id, 1);
    this.setState({flashMsgs});
  }

  onEdit(e) {
    e.preventDefault();
    this.setState({view: 'editTask'});
  }

  onEditCancel() {
    this.setState({view: 'task'});
  }

  onEditSubmit(title, estMins, startOn, desc) {
    this.taskStore.editTask(this.listId, this.taskId, title, estMins, startOn, desc)
    .then(resp => handleFetch(resp))
    .then(task => this.refresh(task))
    .catch(ex => {
      console.error('Unable to edit this task');
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      console.debug(flashMsgs);
      this.setState({
        loading: false,
        flashMsgs: flashMsgs,
        view: 'task'
      });
    });
  }

  onDelete(e) {
    e.preventDefault();
    this.taskStore.deleteTask(this.listId, this.taskId)
    .then(resp => {
      let parent = '/lists/' + this.listId + '/tasks';
      this.context.redirect(parent);
    })
    .catch(ex => {});
  }

  componentDidMount() {
    this.listStore.getList(this.props.listId)
    .then(resp => handleFetch(resp))
    .then(list => {
      this.list = list
      return this.taskStore.getTask(this.listId, this.taskId);
    })
    .then(resp => handleFetch(resp))
    .then(task => this.refresh(task))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        loading: false,
        flashMsgs: flashMsgs,
        view: 'error'
      });
    });
  }

  refresh(task) {
    let estTime = 'No Estimate';
    if (task.estimated_minutes) {
      let est = minsToHrs(task.estimated_minutes);
      estTime = est.hrs + 'h : ' + est.mins + 'm';
    }
    task.estTime = estTime;

    let startOn = 'Not Scheduled';
    if (task.scheduled_start_on) {
      startOn = dateToString(new Date(task.scheduled_start_on));
    }
    task.startOn = startOn;

    this.setState({task: task, loading: false, view: 'task'});
  }

  renderFlashMsgs() {
    let flashes = [];
    for (let i = 0; i < this.state.flashMsgs.length; i++) {
      let flashMsg = this.state.flashMsgs[i];
      flashes.push(<Flash key={i+1} id={i} onClick={this.onFlashClick} type="error" msg={flashMsg} />);
    }
    return flashes;
  }

  renderTaskView() {
    let btns = null;
    if (this.list.state === 'active') {
      btns = (
        <div className="row">
          <div className="col-md-6">
            <Button 
              style={{type: 'btn-default'}} 
              cssStyle={{marginRight: '10px'}}
              onClick={this.onEdit} 
              value='Edit' />
            <Button
              style={{type: 'btn-danger'}}
              onClick={this.onDelete}
              value='Delete' />
          </div>
        </div>
      );
    }

    return(
      <div className="container">
        {this.renderFlashMsgs()}
        <PageHeader value={this.state.task.title} />
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label>List</label>
              <p>{this.state.task.list_name}</p>
            </div>
            <div className="form-group">
              <label>Estimated Time</label>
              <p>{this.state.task.estTime}</p>
            </div>
            <div className="form-group">
              <label>Start On</label>
              <p>{this.state.task.startOn}</p>
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label>State</label>
              <p>{this.state.task.state}</p>
            </div>
            <div className="form-group">
              <label>Description</label>
              <p>{this.state.task.description}</p>
            </div>
          </div>
        </div>
        {btns}
      </div>
    );
  }

  render() {
    if (this.state.loading) {
      return (<PageSpinner pageHeader='Loading' />);
    } 

    if (this.state.view === 'task') {
      return this.renderTaskView();
    } else if (this.state.view === 'editTask') {
      return (
        <TaskForm 
            listName={this.state.task.list_name} 
            onCancel={this.onEditCancel} 
            onSubmit={this.onEditSubmit} 
            task={this.state.task} />
      );
    } else if (this.state.view === 'error') {
      return (
        <div>
          {this.renderFlashMsgs()}
        </div>
      );
    }
  }

}

TaskDetails.propTypes = {
  listId: PropTypes.number.isRequired,
  taskId: PropTypes.number.isRequired
};

TaskDetails.contextTypes = {
  redirect: PropTypes.func
};

export default TaskDetails;