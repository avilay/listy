import React, { Component } from 'react';
import Link from '../router/Link';

class ArchivedTask extends Component {
  
  render() {
    let link = '/lists/' + this.props.listId + '/tasks/' + this.props.task.id;
    if (this.props.task.state === 'done') {
      return(
        <div>
          <div className="row">
            <div className="col-xs-4 col-sm-8 text-muted">
              <Link href={link} className="title"><s>{this.props.task.title}</s></Link>
            </div>
          </div>
          <hr />
        </div>
      ); 
    } else {
      return(
        <div>
          <div className="row">
            <div className="col-xs-4 col-sm-8">
              <Link href={link} className="title">{this.props.task.title}</Link>
            </div>
          </div>
          <hr />
        </div>
      );
    }
  }
}

export default ArchivedTask;