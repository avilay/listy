import React, { Component, PropTypes } from 'react';

class Checkbox extends Component {

  render() {
    if (this.props.isSpinning) {
      return (
          <i className="fa fa-spinner fa-spin check"></i>
      );
    } else {
      return (
          <i className="fa fa-square-o fa-lg check" onClick={this.props.onClick}></i>
      );

    }
  }

}

Checkbox.propTypes = {
  isSpinning: PropTypes.bool,
  onClick: PropTypes.func
}


export default Checkbox;