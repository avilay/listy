import React from 'react';
import Task from './Task';
import Link from '../router/Link';

class DoneTask extends Task {

  render() {
    let link = '/lists/' + this.props.listId + '/tasks/' + this.props.task.id;

    return(
      <div>
        <div className="row">
          <div className="col-xs-7 col-sm-10 text-muted">
            <i className="fa fa-check-square-o fa-lg check"></i>
            <Link href={link} className="title"><s>{this.props.task.title}</s></Link>
          </div>
        </div>
        <hr />
      </div>
    );
  }

}


export default DoneTask;