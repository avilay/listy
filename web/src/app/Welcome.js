import React, { Component } from 'react';
import PageHeader from './common/PageHeader';

class Welcome extends Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.preventDefault();
    console.log('Button clicked');
    window.location = '/hahaha';
  }

  render() {
    return (
      <div className="container">
        <PageHeader value="Welcome" />
        <button className="btn btn-default" onClick={this.onClick}>Click Me!</button>
      </div>
    );
  }
}

export default Welcome;