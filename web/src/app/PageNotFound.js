import React, { Component } from 'react';
import PageHeader from './common/PageHeader';

class PageNotFound extends Component {

  render() {
    return (
      <div className="container">
        <PageHeader value="Page Not Found" />
        <div>The page you requested is not not part of Listy.</div>
      </div>
    );
  }
}

export default PageNotFound;