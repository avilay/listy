import React, { Component, PropTypes } from 'react';
import createUserStore from './store/UserStore';
import Welcome from './Welcome';
import Lists from './lists/Lists';
import Scheduled from './tasks/Scheduled';
import Navbar from './common/Navbar';
import Tasks from './tasks/Tasks';
import TaskDetails from './tasks/TaskDetails';
import Login from './user/Login';
import Register from './user/Register';
import Logout from './user/Logout';
import createRouter from './router/Router';
import PageNotFound from './PageNotFound';

class App extends Component {

  constructor(props) {
    super(props);
    console.debug('App::ctor');

    this.userStore = createUserStore();

    this.router = createRouter();
    this.router.add('/welcome', 'welcome', []);
    this.router.add('/login', 'login', []);
    this.router.add('/logout', 'logout', []);
    this.router.add('/register', 'register', []);
    this.router.add('/lists', 'lists', []);
    this.router.add('/lists/(\\d+)/tasks', 'tasks', ['listId'])
    this.router.add('/lists/(\\d+)/tasks/(\\d+)', 'task', ['listId', 'taskId']);
    this.router.add('/scheduled', 'scheduled', []);

    this.state = {
      view: 'welcome',
      params: {},
      nonce: 1
    };

    this.redirect = this.redirect.bind(this);
    this.onLoggedIn = this.onLoggedIn.bind(this);
    this.onBrowserPopState = this.onBrowserPopState.bind(this);

    window.onpopstate = this.onBrowserPopState;
  }

  getChildContext() {
    return {redirect: this.redirect};
  }

  redirect(link) {
    window.history.pushState(null, null, link);
    let newView = this.router.match(link);
    this.setState({view: newView.view, params: newView.params});
  }

  onLoggedIn() {
    if (this.state.view === 'login') {
      return this.redirect('/scheduled');
    } else {
      let newNonce = this.state.nonce + 1;
      this.setState({nonce: newNonce});
    }
  }

  componentDidMount() {
    console.debug('App::componentDidMount');
    let link = window.location.pathname;
    if (link !== '/') {
      this.redirect(window.location.pathname);
    }
  }

  onBrowserPopState() {
    let newView = this.router.match(window.location.pathname);
    this.setState({view: newView.view, params: newView.params});
  }

  render() {
    let navbar = (<Navbar />);
    let component = null;

    if (this.userStore.isAuthenticated()) {
      navbar = (<Navbar username={this.userStore.getUser().name} />);
    }

    if (this.state.view === 'welcome') {
      component = (<Welcome />);
    } else if (this.state.view === 'login') {
      component = (<Login onLoggedIn={this.onLoggedIn} />);
    } else if (this.state.view === 'logout') {
      component = (<Logout />);
    } else if (this.state.view === 'register') {
      component = (<Register />);
    } else if (this.state.view === 'pageNotFound') {
      component = (<PageNotFound />);
    } else if (this.userStore.isAuthenticated()) {
      if (this.state.view === 'lists') {
        component = (<Lists />);
      } else if (this.state.view === 'tasks') {
        let listId = parseInt(this.state.params.listId, 10);
        component = (<Tasks listId={listId} />);
      } else if (this.state.view === 'scheduled') {
        component = (<Scheduled />);
      } else if (this.state.view === 'task') {
        let listId = parseInt(this.state.params.listId, 10);
        let taskId = parseInt(this.state.params.taskId, 10);
        component = (<TaskDetails listId={listId} taskId={taskId} />);
      }  
    } else {
      component = (<Login onLoggedIn={this.onLoggedIn} />);
    }  
    
    return (
      <div>
        {navbar}
        {component}
      </div>
    );
  }

}

App.childContextTypes = {
  redirect: PropTypes.func
};

export default App;
