import React, { Component, PropTypes } from 'react';
import Link from '../router/Link';

class List extends Component {

  constructor(props) {
    super(props);
    this.onEdit = this.onEdit.bind(this);
  }

  onEdit(e) {
    e.preventDefault();
    this.props.onEdit(this.props.list.id);
  }

  render() {
    let taskLink = '/lists/' + this.props.list.id + '/tasks';
    return (
      <div className="row">
        <div className="col-md-7 list">
          <div className="col-md-10">
            <Link href={taskLink} className="title">{this.props.list.name}</Link>
          </div>
          <div onClick={this.onEdit} className="col-md-2">
            <i className="fa fa-pencil edit" aria-hidden="true"></i>
          </div>
        </div>
      </div>
    );
  }

}

List.propTypes = {
  list: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  onEdit: PropTypes.func
};

export default List;