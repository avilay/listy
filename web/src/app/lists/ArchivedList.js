import React, { Component, PropTypes } from 'react';
import Link from '../router/Link';

class ArchivedList extends Component {

  constructor(props) {
    super(props);
    this.state = {deleting: false};
    this.onDelete = this.onDelete.bind(this);
  }

  onDelete(e) {
    e.preventDefault();
    this.setState({deleting: true});
    this.props.onDelete(this.props.list.id);
  }

  render() {
    let taskLink = '/lists/' + this.props.list.id + '/tasks';

    let delBtn = (
      <div onClick={this.onDelete} className="col-md-1 col-xs-1">
        <i className="fa fa-trash trash" aria-hidden="true"></i>
      </div>
    );
    if (this.state.deleting) {
      delBtn = (
        <div className="col-md-2 col-xs-3">
          <i className="fa fa-spinner fa-spin" aria-hidden="true"></i>
        </div>
      );
    }

    return (
      <div className="row">
        <div className="col-md-7 list text-muted">
          <div className="col-md-10 col-xs-9">
            <Link href={taskLink} className="title">{this.props.list.name}</Link>
          </div>
          {delBtn}
        </div>
      </div>
    );
  }

}

ArchivedList.propTypes = {
  list: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  onDelete: PropTypes.func.isRequired
};

export default ArchivedList;