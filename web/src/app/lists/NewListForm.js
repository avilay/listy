import React, { Component, PropTypes } from 'react';
import Button from '../common/Button';

class NewListForm extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      submitting: false,
      listName: ''
    };
    this.onListNameChange = this.onListNameChange.bind(this);
  }

  onListNameChange(e) {
    this.setState({listName: e.target.value});
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({submitting: true});
    this.props.onSubmit(this.state.listName);
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-10">
          <form className="form-inline" onSubmit={this.onSubmit}>
            <div className="form-group">
              <input 
                type="text" 
                className="form-control" 
                placeholder="List Name" 
                value={this.state.listName} 
                onChange={this.onListNameChange} >
              </input>
            </div>
            <div className="form-group">
              <Button type="submit"
                style={{type: 'btn-success'}}
                value="Create" 
                isSpinning={this.state.submitting} />
            </div>
            <div className="form-group">
              <Button style={{type: 'btn-default'}}
                value="Cancel"
                onClick={this.props.onCancel} />
            </div>
          </form>
        </div>
      </div>
    );
  }

}

NewListForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

export default NewListForm;