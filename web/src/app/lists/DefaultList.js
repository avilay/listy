import React, { Component, PropTypes } from 'react';
import Link from '../router/Link';

class DefaultList extends Component {

  render() {
    let taskLink = '/lists/' + this.props.list.id + '/tasks';    
    return (
      <div className="row">
        <div className="col-md-7 list">
          <div className="col-md-6 col-xs-9">
            <Link href={taskLink} className="title">{this.props.list.name}</Link>
          </div>
        </div>
      </div>
    );
  }
}

DefaultList.propTypes = {
  list: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired
}

export default DefaultList;