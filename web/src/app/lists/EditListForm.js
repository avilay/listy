import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Button from '../common/Button';

class EditListForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      submitting: false, 
      archiving: false, 
      deleting: false,
      listName: this.props.list.name
    };

    this.onListNameChange = this.onListNameChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onArchive = this.onArchive.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  onListNameChange(e) {
    this.setState({listName: e.target.value});
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({submitting: true});
    this.props.onSubmit(this.props.list.id, this.state.listName);
  }

  onArchive(e) {
    e.preventDefault();
    this.setState({archiving: true});
    this.props.onArchive(this.props.list.id);
  }

  onDelete(e) {
    e.preventDefault();
    this.setState({deleting: true});
    this.props.onDelete(this.props.list.id);
  }

  componentDidMount(){
    ReactDOM.findDOMNode(this.refs.listName).focus(); 
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-10">
          <form className="form-inline edit-list" onSubmit={this.onSubmit}>
            <div className="form-group">
              <input 
                type="text" 
                className="form-control input-sm" 
                ref="listName"
                value={this.state.listName}
                onChange={this.onListNameChange} >
              </input>
            </div>
            <div className="form-group">
              <Button type="submit"
                style={{type: 'btn-success', size: 'btn-sm'}}
                icon={{type: 'fa-check'}}
                isSpinning={this.state.submitting} />
            </div>
            <div className="form-group">
              <Button style={{type: 'btn-primary', size: 'btn-sm'}}
                icon={{type: 'fa-archive'}}
                isSpinning={this.state.archiving}
                onClick={this.onArchive} />
            </div>
            <div className="form-group">
              <Button style={{type: 'btn-danger', size: 'btn-sm'}}
                icon={{type: 'fa-trash'}}
                isSpinning={this.state.deleting}
                onClick={this.onDelete} />
            </div>
            <div className="form-group">
              <Button style={{type: 'btn-default', size: 'btn-sm'}}
                icon={{type: 'fa-times'}}
                onClick={this.props.onCancel} />
            </div>
          </form>
        </div>
      </div>
    );
  }

}

EditListForm.propTypes = {
  list: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onArchive: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default EditListForm;