import React, { Component } from 'react';
import List from './List';
import DefaultList from './DefaultList';
import ArchivedList from './ArchivedList';
import NewItemButton from '../common/NewItemButton';
import NewListForm from './NewListForm';
import EditListForm from './EditListForm';
import PageSpinner from '../common/PageSpinner';
import PageHeader from '../common/PageHeader';
import createListStore from '../store/ListStore';
import Flash from '../common/Flash';
import { handleFetch, genFlashMsg } from '../utils';

class Lists extends Component {

  constructor(props) {
    super(props);
    this.store = createListStore();

    this.state = {
      flashMsgs: [],
      view: 'lists',
      isLoading: true,
      editListId: -1,
      defaultList: null,
      userGenLists: null
    };

    this.refreshOne = this.refreshOne.bind(this);
    this.refreshAll = this.refreshAll.bind(this);

    this.onNewList = this.onNewList.bind(this);
    this.onNewListCancel = this.onNewListCancel.bind(this);
    this.onNewListSubmit = this.onNewListSubmit.bind(this);

    this.onEdit = this.onEdit.bind(this);
    this.onEditSubmit = this.onEditSubmit.bind(this);
    this.onArchive = this.onArchive.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onEditCancel = this.onEditCancel.bind(this);

    this.onFlashClick = this.onFlashClick.bind(this);
  }

  componentDidMount() {
    this.store.getLists()
    .then(resp => handleFetch(resp))
    .then(lists => this.refreshAll(lists))
    .catch(ex => {
      console.debug('Lists::componentDidMount got an error', ex.name);
      let flashMsg = genFlashMsg(ex);
      console.debug('flash msg', flashMsg);
      this.setState({
        flashMsgs: [flashMsg],
        isLoading: false,
        view: 'error'
      })
    });
  }

  onNewList(e) {
    e.preventDefault();
    this.setState({view: 'newList'});
  }

  onNewListCancel(e) {
    e.preventDefault();
    this.setState({view: 'lists'});
  }

  onNewListSubmit(name) {
    this.store.createList(name)
    .then(resp => handleFetch(resp))
    .then(newList => this.refreshOne(newList))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        flashMsgs: flashMsgs,
        isLoading: false,
        view: 'lists'
      });
    });
  }

  onEdit(listId) {
    this.setState({view: 'editList', editListId: listId});
  }

  onEditCancel(e) {
    e.preventDefault();
    this.setState({view: 'lists', editListId: -1});
  }

  onEditSubmit(listId, listName) {
    this.store.updateListName(listId, listName)
    .then(resp => handleFetch(resp))
    .then(updatedList => this.refreshOne(updatedList))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        flashMsgs: flashMsgs,
        isLoading: false,
        editListId: -1
      });
    });
  }

  onArchive(listId) {
    this.store.archiveList(listId)
    .then(resp => handleFetch(resp))
    .then(archivedList => this.refreshOne(archivedList))
    .catch(ex => {
      let flashMsgs = this.state.flashMsgs;
      flashMsgs.push(genFlashMsg(ex));
      this.setState({
        flashMsgs: flashMsgs,
        isLoading: false,
        editListId: -1
      })
    });
  }

  onDelete(listId) {
    this.store.deleteList(listId)
    .then(resp => {
      let userGenLists = this.state.userGenLists;
      userGenLists.delete(listId);

      this.setState({
        view: 'lists',
        editListId: -1,
        userGenLists: userGenLists
      });
    })
    .catch(ex => {});
  }

  onFlashClick(id) {
    let flashMsgs = this.state.flashMsgs;
    flashMsgs.splice(id, 1);
    this.setState({flashMsgs});
  }

  refreshAll(lists) {
    let defaultList = lists.default_;

    let userGenLists = new Map();
    for (let list of lists.usergen) {
      userGenLists.set(list.id, list);
    }
    
    this.setState({
      isLoading: false,
      defaultList: defaultList,
      userGenLists: userGenLists
    });
  }

  refreshOne(list) {
    let lists = this.state.userGenLists;
    lists.set(list.id, list);
    
    this.setState({
      isLoading: false,
      editListId: -1,
      view: 'lists',
      userGenLists: lists
    });
  }

  splitLists() {
    let activeLists = [];
    let archivedLists = [];
    
    for (let kv of this.state.userGenLists) {
      let list = kv[1];
      if (list.state === 'active') {
        if (list.id === this.state.editListId) {
          activeLists.push(
            <EditListForm key={list.id} 
              list={list} 
              onSubmit={this.onEditSubmit}
              onArchive={this.onArchive}
              onDelete={this.onDelete}
              onCancel={this.onEditCancel} />
            );
        } else {
          activeLists.push(
            <List key={list.id} list={list} onEdit={this.onEdit} setAppView={this.props.setAppView} />
          );
        }
      } else if (list.state === 'archived') {
        archivedLists.push(
          <ArchivedList key={list.id} list={list} onDelete={this.onDelete} setAppView={this.props.setAppView} />
        );
      }
    }

    return [activeLists, archivedLists];
  }

  renderFlashMsgs() {
    let flashes = [];
    for (let i = 0; i < this.state.flashMsgs.length; i++) {
      let flashMsg = this.state.flashMsgs[i];
      flashes.push(<Flash key={i+1} id={i} onClick={this.onFlashClick} type="error" msg={flashMsg} />);
    }
    return flashes;
  }

  renderEditListView() {
    let [activeLists, archivedLists] = this.splitLists();

    return (
      <div className="container">
        {this.renderFlashMsgs()}
        <PageHeader value="Lists" />
        <DefaultList list={this.state.defaultList} setAppView={this.props.setAppView} />
        {activeLists}
        <PageHeader value="Archived Lists" />
        {archivedLists}
      </div>
    );
  }

  renderNewListView() {
    let [activeLists, archivedLists] = this.splitLists();

    return (
      <div className="container">
        {this.renderFlashMsgs()}
        <PageHeader value="Lists" />
        <NewListForm
          onCancel={this.onNewListCancel}
          onSubmit={this.onNewListSubmit} />
        <DefaultList list={this.state.defaultList} />
        {activeLists}
        <PageHeader value="Archived Lists" />
        {archivedLists}
      </div>
    );
  }

  renderListsView() {
    let [activeLists, archivedLists] = this.splitLists();

    return (
      <div className="container">
        {this.renderFlashMsgs()}
        <PageHeader value="Lists" />
        <NewItemButton onClick={this.onNewList} value='New List' />
        <DefaultList list={this.state.defaultList} />
        {activeLists}
        <PageHeader value="Archived Lists" />
        {archivedLists}
      </div>
    );
  }

  render() {
    if (this.state.isLoading) {
      return (<PageSpinner pageHeader="Lists" />);
    }

    if (this.state.view === 'lists') {
      return this.renderListsView();
    } else if (this.state.view === 'newList') {
      return this.renderNewListView();
    } else if (this.state.view === 'editList') {
      return this.renderEditListView();
    } else if (this.state.view === 'error') {
      return (
        <div className="container">
          {this.renderFlashMsgs()}
        </div>
      );
    }

  }
}

export default Lists;
