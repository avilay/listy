class Router {
  constructor() {
    this.routes = new Map();
    this.order = [];
  }

  add(pathre, view, params) {
    this.routes.set(pathre, {view, params});
    this.order.push(pathre);
    this.order.sort((a, b) => {
      if (a.length < b.length) {
        return 1;
      } else if (a.length === b.length) {
        return 0;
      } else {
        return -1;
      }
    })
  }

  match(path) {
    for (let key of this.order) {
      let pathre = new RegExp(key, 'i');
      let matches = path.match(pathre);
      let paramNames = this.routes.get(key).params;

      if (matches && matches.length > paramNames.length) {
        let view = this.routes.get(key).view;
        let viewState = {view: view, params: {}};
        for (let i = 0; i < paramNames.length; i++) {
          viewState.params[paramNames[i]] = matches[i+1];
        }
        return viewState;
      }
    }

    return {view: 'pageNotFound'};
  }

}

let instance = null;
let createRouter = () => {
  if (instance === null) {
    instance = new Router();
  }
  return instance;
}

export default createRouter;
// let router = new Router();
// router.add('/lists', 'lists', []);
// router.add('/lists/(\\d+)/tasks', 'tasks', ['listId'])
// router.add('/lists/(\\d+)/tasks/(\\d+)', 'task', ['listId', 'taskId']);

// let view = router.match('/lists');
// console.log(view);

// view = router.match('/lists/1/tasks');
// console.log(view);

// view = router.match('/lists/1/tasks/1');
// console.log(view);

// view = router.match('/lists/1');
// console.log(view);
