import React, { Component, PropTypes } from 'react';
import createRouter from './Router';

class Link extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.router = createRouter();
  }

  onClick(e) {
    e.preventDefault();
    this.context.redirect(this.props.href);
  }

  render() {
    return (
      <a className={this.props.className} href={this.props.href} onClick={this.onClick}>
        {this.props.children}
      </a>
    );
  }
}

Link.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string
};

Link.contextTypes = {
  redirect: PropTypes.func
};


export default Link;