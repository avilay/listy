class UserStore {
  
  constructor() {
    console.debug('UserStore ctor');
    //this.endpoint = 'http://localhost:7070';
    this.endpoint = 'http://listyapi.avilay.info'
    this.user = {};
    this.authenticated = false;
  }

  login(email, password) {
    let url = '/login'
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({email, password})
    };
    return fetch(fullUrl, config);
  }

  getUser() {
    return this.user;
  }

  isAuthenticated() {
    return this.authenticated;
  }

  setUser(user) {
    this.user = user;
    this.authenticated = true;
  }

  removeUser() {
    this.user = {};
    this.authenticated = false;
  }

}

let instance = null;
let createUserStore = () => {
  if (instance === null) {
    instance = new UserStore();
  }
  return instance;
}


export default createUserStore;