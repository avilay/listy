import createUserStore from './UserStore';

class TaskStore {

  constructor() {
    this.user = createUserStore().getUser();
//    this.endpoint = 'http://localhost:7070';
    this.endpoint = 'http://listyapi.avilay.info'
  }

  getScheduledTasks() {
    let url = '/scheduled-tasks';
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Accept': 'application/json'
      }
    };
    return fetch(fullUrl, config);
  }

  getTasks(listId) {
    let url = '/lists/' + listId + '/tasks?filter=recent';
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Accept': 'application/json'
      }
    };
    return fetch(fullUrl, config);
  }

  getTask(listId, taskId) {
    let url = '/lists/' + listId + '/tasks/' + taskId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Accept': 'application/json'
      }
    };
    return fetch(fullUrl, config);
  }

  changeTaskState(listId, taskId, newState) {
    let url = '/lists/' + listId + '/tasks/' + taskId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'PATCH',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({state: newState})
    };
    return fetch(fullUrl, config);
  }

  editTask(listId, taskId, task) {
    console.debug('TaskStore::editTask', task);
    let url = '/lists/' + listId + '/tasks/' + taskId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'PATCH',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(task)
    };
    return fetch(fullUrl, config);
  }

  deleteTask(listId, taskId) {
    let url = '/lists/' + listId + '/tasks/' + taskId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'DELETE',
      headers: {
        'Authorization': 'Basic ' + this.user.token
      }
    };
    return fetch(fullUrl, config);
  }

  createTask(listId, task) {
    console.debug('TaskStore::createTask', task);
    let url = '/lists/' + listId + '/tasks';
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(task)
    };
    return fetch(fullUrl, config);
  }
}

let instance = null;
let createTaskStore = () => {
  if (instance === null) {
    instance = new TaskStore();
  }
  return instance;
};


export default createTaskStore;