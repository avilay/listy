import createUserStore from './UserStore';

class ListStore {

  constructor() {
    this.user = createUserStore().getUser();
//    this.endpoint = 'http://localhost:7070';
    this.endpoint = 'http://listyapi.avilay.info'
  }

  createList(name) {
    let url = '/lists';
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({name: name})
    };
    return fetch(fullUrl, config);
  }

  getLists() {
    let url = '/lists';
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Accept': 'application/json'
      }
    };
    return fetch(fullUrl, config);
  }

  getList(listId) {
    let url = '/lists/' + listId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Accept': 'application/json'
      }
    };
    return fetch(fullUrl, config);
  }

  updateListName(listId, name) {
    let url = '/lists/' + listId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'PATCH',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({name: name})
    };
    return fetch(fullUrl, config);
  }

  archiveList(listId) {
    let url = '/lists/' + listId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'PATCH',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({state: 'archived'})
    };
    return fetch(fullUrl, config);
  }
  
  deleteList(listId) {
    let url = '/lists/' + listId;
    let fullUrl = this.endpoint + url;
    let config = {
      method: 'DELETE',
      headers: {
        'Authorization': 'Basic ' + this.user.token,
      }
    };
    return fetch(fullUrl, config);
  }

}

let instance = null;
let createListStore = () => {
  if (instance === null) {
    instance = new ListStore();
  }
  return instance;
}


export default createListStore;