let months = [
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];

export function dateToString(dt) {
  let day = dt.getDate().toString();
  let mon = months[dt.getMonth()];
  return day + ' ' + mon;
}

function paddedIntToStr(i) {
  if (i < 10) {
    return '0' + i.toString();
  } else {
    return i.toString();
  }
}

export function dateToMmDdYyyy(dt) {
  let mm = paddedIntToStr(dt.getMonth() + 1);
  let dd = paddedIntToStr(dt.getDate());
  let yyyy = dt.getFullYear().toString();
  return mm + '/' + dd + '/' + yyyy;
}

export function minsToHrs(totMins) {
  let hrs = Math.floor(totMins / 60);
  let mins = totMins % 60;
  return {hrs, mins};
}

export function calcMinsSpent(task) {
  let minsSinceLastStart = (new Date().getTime() - new Date(task.last_start_on).getTime()) / (60 * 1000);
  return Math.round(task.minutes_spent + minsSinceLastStart);
}

class UnauthorizedError extends Error {
  constructor(message) {
    super(message);
    this.message = message;
    this.name = 'UnauthorizedError';
  }
}


class UserError extends Error {
  constructor(message) {
    super(message);
    this.message = message;
    this.name = 'UserError'; 
  }
}

class ServerError extends Error {
  constructor(message) {
    super(message);
    this.message = message;
    this.name = 'ServerError';
  }
}

class UnknownError extends Error {
  constructor(message) {
    super(message);
    this.message = message;
    this.name = 'UnknownError';
  }
}

export function handleFetch(resp) {
  console.debug(resp);
  if (resp.ok) {
    return resp.json();
  }

  if (resp.status === 401) {
    return Promise.reject(new UnauthorizedError());
  }

  if (resp.status > 401 && resp.status < 500) {
    return Promise.reject(new UserError(resp.statusText));
  }

  if (resp.status >= 500) {
    return Promise.reject(new ServerError());
  }

  return Promise.reject(new UnknownError());
}

export function genFlashMsg(ex) {
  let flashMsg = '';
  if (ex.name === 'TypeError') {
    flashMsg = 'Unable to reach server!';
  } else if (ex.name === 'UnauthorizedError') {
    flashMsg = 'Invalid credentails!';
  } else if (ex.name === 'UserError') {
    flashMsg = ex.message ? ex.message : 'User error';
  }
  else if (ex.name === 'ServerError') {
    flashMsg = 'Internal error!';
  }
  return flashMsg;
}
