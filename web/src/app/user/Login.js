import React, { Component, PropTypes } from 'react';
import PageHeader from '../common/PageHeader';
import Button from '../common/Button';
import Flash from '../common/Flash';
import createUserStore from '../store/UserStore';
import { handleFetch, genFlashMsg } from '../utils';

class Login extends Component {

  constructor(props) {
    super(props);
    this.userStore = createUserStore();

    this.state = {
      flashMsgs: [],
      email: '',
      password: '',
      submitting: false,
      loggedIn: this.userStore.isAuthenticated()
    };

    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.onFlashClick = this.onFlashClick.bind(this);
  }

  onFlashClick(id) {
    let flashMsgs = this.state.flashMsgs;
    flashMsgs.splice(id, 1);
    this.setState({flashMsgs});
  }

  onEmailChange(e) {
    this.setState({email: e.target.value});
  }

  onPasswordChange(e) {
    this.setState({password: e.target.value});
  }

  onSubmit(e) {
    e.preventDefault();
    let flashMsgs = [];

    if (!this.state.email) {
      flashMsgs.push('You must provide an email!');
    }

    if (!this.state.password) {
      flashMsgs.push('You must provide a password!');
    }

    if (flashMsgs.length > 0) {
      this.setState({flashMsgs});
    } else {
      this.setState({submitting: true});
      this.userStore.login(this.state.email, this.state.password)
      .then(resp => handleFetch(resp))
      .then(user => {
        this.userStore.setUser(user);
        this.props.onLoggedIn();
      })
      .catch(ex => {
        let flashMsg = genFlashMsg(ex);
        this.setState({
          submitting: false,
          flashMsgs: [flashMsg],
          loggedIn: false
        });
      });  
    }
  }

  render() {
    let flashes = [];
    for (let i = 0; i < this.state.flashMsgs.length; i++) {
      let flashMsg = this.state.flashMsgs[i];
      flashes.push(<Flash key={i+1} id={i} onClick={this.onFlashClick} type="error" msg={flashMsg} />);
    }
    return (
      <div className="container">
        {flashes}
        <PageHeader value="Login" />
        <div className="row">
          <div className="col-md-4">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label>Email</label>
                <input type="email" className="form-control" value={this.state.email} onChange={this.onEmailChange} />                
              </div>
              <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" value={this.state.password} onChange={this.onPasswordChange} />                
              </div>
              <Button type="submit" style={{type: 'btn-primary'}} value="Login" isSpinning={this.state.submitting} />
            </form>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-md-4">
            <h3>Not a member?</h3>
            <a href="/register" className="btn btn-warning">Register</a>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  onLoggedIn: PropTypes.func.isRequired
};

export default Login;