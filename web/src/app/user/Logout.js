import React, { Component } from 'react';
import PageHeader from '../common/PageHeader';
import createUserStore from '../store/UserStore';

class Logout extends Component {

  constructor(props) {
    super(props);
    this.userStore = createUserStore();
  }

  componentDidMount() {
    this.userStore.removeUser();
  }

  render() {
    return (
      <div className="container">
        <PageHeader value="Logout" />
        <div>You have been logged out of Listy.</div>
      </div>
    );
  }
}

export default Logout;