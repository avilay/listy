import React from 'react';
import { render } from 'react-dom';
import App from './app/App';

import './css/font-awesome.min.css';
import './css/bootstrap.min.css';
import './css/bootstrap-theme.min.css';
import './css/full_top_navbar.css';
import './css/listy.css';
import './css/datetime.css';

render(
  <App />,
  document.getElementById('root')
);
