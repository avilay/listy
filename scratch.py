from api.repository.pg.pg_repository_old import PgRepository
from api.repository.repo_exceptions import RepoExceptionError


def main():
    config = dict(
        POSTGRES=dict(
            host='localhost',
            database='listy',
            user='avilay.parekh'
        )
    )
    repo = PgRepository(config)

    ulists = repo.create_list(1, 1000)
    print(ulists)

if __name__ == '__main__':
    main()
